(function ($, Drupal, drupalSettings) {
  'use strict';
  let recurlyJsForm = Drupal.behaviors.recurlyCheckoutPaypal = {
    data: {
      token: null
    },

    attach: function (context) {
      const $checkoutForm =
        $('form.commerce-checkout-flow.payment-redirect-form', context);
      $checkoutForm.once('recurly-js-form-credit-card').each(() =>
        recurlyJsForm.submitHandler($checkoutForm));
    },

    submitHandler: function ($checkoutForm) {
      $checkoutForm.on('submit', function (e) {
        if (recurlyJsForm.data.token) {
          return;
        }

        e.preventDefault();
        Drupal.behaviors.commerceRecurlyCheckout.initClient(drupalSettings)
        const paypal = recurly.PayPal({
          display: {
            // This must end up being 127 bytes (i.e. characters) or less.
            displayName: 'You will be charged ' + recurlyJsForm.data.order_total + '. If your order contains a subscription then this payment method will be used for future charges.'
          }
        });

        paypal
          .on('token', function (token) {
            // token.id$
            // alert('token!')
            // console.log('token: ', token.id, token)
            recurlyJsForm.data.token = token;
            $('#field_recurly_token_id').val(token.id)
            $('#field_recurly_token_type').val(token.type)
            $checkoutForm.submit();
          })
          .on('error', function (err) {
            console.log(err.message, err)
            // err.code
            // err.message
            // [err.cause] if there is an embedded error
          })
          .start()
      })
    }
  };

}(jQuery, Drupal, drupalSettings));
