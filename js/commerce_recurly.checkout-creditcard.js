(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.recurlyCheckoutCreditCard = {
    attach: function (context) {
      let recurlyForm = function () {

        // Make sure that recurly.js cc validation only happens
        // in the context of an actual cc form. Since we are
        // attaching all of the js files in a generic manner
        // at the moment, this file ends up being loaded in
        // the context of paypal checkout as well. By checking
        // that the hosted-field exists, we ensure that paypal
        // submissions do not hit validation errors related to
        // credit cards forms, such as missing fields.
        const $rjsHostedField = $('.recurly-hosted-field', context);

        if (!$rjsHostedField.length) {
          return;
        }


        const $billingAddressField = $('.field--type-address', context);
        if ($billingAddressField.length) {
          $('#recurly-name').hide();
        }

        // Get billing token before
        $('form.commerce-checkout-flow').once('recurly-js-form-token-submit').each(function () {
          $(this).on('submit', function (event) {

            const selectedPaymentMethod = $('input[name="payment_information[payment_method]"]:checked').val();
            if (selectedPaymentMethod && !selectedPaymentMethod.includes('credit_card')) {
              return;
            }

            Drupal.behaviors.recurlyCheckoutCreditCard.processing = true;
            event.preventDefault();

            // If using a pre-existing billing profile address, we need to
            // populate our hidden data-recurly fields with that info.
            const $existingBillingInfo = $('[data-drupal-selector="edit-payment-information-add-payment-method-billing-information-rendered"]');

            if ($existingBillingInfo.length) {
              const address = {
                'first_name': $existingBillingInfo.find('.given-name').first().text(),
                'last_name': $existingBillingInfo.find('.family-name').first().text(),
                'address1': $existingBillingInfo.find('.address-line1').first().text(),
                'address2': $existingBillingInfo.find('.address-line2').first().text(),
                'city': $existingBillingInfo.find('.locality').first().text(),
                'state': $existingBillingInfo.find('.administrative-area').first().text(),
                'postal_code': $existingBillingInfo.find('.postal-code').first().text(),
                'country': $existingBillingInfo.find('.country').first().text(),
              }

              $('[data-recurly="first_name"]').val(address.first_name);
              $('[data-recurly="last_name"]').val(address.last_name);
              $('[data-recurly="address1"]').val(address.address1);
              $('[data-recurly="address2"]').val(address.address2);
              $('[data-recurly="city"]').val(address.city);
              $('[data-recurly="state"]').val(address.state);
              $('[data-recurly="postal_code"]').val(address.postal_code);
              $('[data-recurly="country"]').val(address.country);
            }

            let recurly_form = this;

            recurly.token(recurly_form, function (error, token) {
              if (error) {
                console.log('Recurly token error');
                console.log(error);
              } else {
                recurly_form.submit();
              }
            });
          });
        });

      };

      // If an existing payment method is selected then this
      // lib won't have been attached, since it's appended by
      // the payment method forms.
      if (typeof drupalSettings.commerce_recurly === 'undefined') {
        return;
      }

      Drupal.behaviors.commerceRecurlyCheckout.initClient(drupalSettings)
      $('.checkout-pane', context).once('recurly-js-form').each(recurlyForm);
    }
  };

}(jQuery, Drupal, drupalSettings));
