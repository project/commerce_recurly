/**
 * Common methods needed for recurlyjs checkout js files.
 */
(function ($, Drupal) {
  'use strict';

  var crc = Drupal.behaviors.commerceRecurlyCheckout = {
    initClient: function (drupalSettings) {
      var data = JSON.parse(drupalSettings.commerce_recurly);
      var public_key = data.public_key;

      crc.waitForRecurly(function () {
        recurly.configure(public_key);
      })
    },

    waitForRecurly: function (callback) {
      var waitForGlobal = function (key, callback) {
        if (window[key]) {
          callback();
        }
        else {
          setTimeout(function () {
            waitForGlobal(key, callback);
          }, 100);
        }
      };

      waitForGlobal("recurly", function () {
        // Initialize recurlyjs with public key
        return callback();
      });
    },
  };

}(jQuery, Drupal));
