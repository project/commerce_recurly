Since this is still a sandbox project, documentation is sparse.

However, one thing to note is that keeping track of remote_ids for orders and payments is a bit messy. What this means
is that the remote_id set on payments is the ID of a particular recurly charge. However, if the purchase is for a
subscription trial, then there will be not purchase.

To accommodate multiple situations, the invoice_id associated with the purchase also needs to be tracked. Since there is
not a good way for this module to install a field to do this, you should manually add a field to your order types that
are/may be purchased via recurly to track the invoice ID. This should be named "field_invoice_id" and be of type "
text (plain)". If this field is present when the purchase is generated for an order, the associated invoice ID will be
saved to it.

Similarly, you can add "field_subscription_ids" field of type "text (plain)", and that will associate an order with
any subscriptions that it purchases.
