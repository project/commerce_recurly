<?php

namespace Drupal\commerce_recurly;

use Drupal\Core\Database\Connection;

/**
 * Class Accounts.
 */
class Accounts implements AccountsInterface {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var Connection $database
   */
  protected Connection $database;

  /**
   * Constructs a new Accounts object.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  public function getAccountCodes($uid, $plan_type = '') {
    $query = $this->database->select('commerce_recurly_accounts', 'cra');
    $query->fields('cra', ['recurly_account_code']);
    $query->condition('cra.uid', $uid, '=');
    if ($plan_type) {
      $query->condition('cra.type', $plan_type, '=');
    }
    return $query->execute()->fetchAll();
  }

}
