<?php

namespace Drupal\commerce_recurly;

use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_recurly\Traits\AccountTrait;
use Drupal\commerce_recurly\Traits\CustomFieldsTrait;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Recurly\Errors\NotFound;

/**
 * Provides utilities to/for commerce_recurly.
 */
class Utils implements UtilsInterface {

  use StringTranslationTrait;
  use AccountTrait;
  use CustomFieldsTrait;

  /**
   * The recurly Client.
   *
   * @var \Recurly\Client
   */
  protected $recurlyClient;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private EntityTypeManager $entityTypeManager;

  // this is provided by AccountTrait
  private array $configuration;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Utility\Token
   */
  private Token $token;

  /**
   * Utils constructor.
   *
   * @param \Drupal\commerce_recurly\RecurlyClient $recurly_client_service
   *   The recurly client service.
   */
  public function __construct(
    RecurlyClient        $recurly_client_service,
    EntityTypeBundleInfo $entity_type_bundle_info,
    EntityFieldManager   $entity_field_manager,
    MessengerInterface   $messenger,
    EntityTypeManager    $entity_type_manager,
    Token                $token) {
    $this->recurlyClient = $recurly_client_service->initDefault();
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->token = $token;
  }

  /**
   * {@inheritDoc}
   */
  public function determineNonplanDelta(string $account_code): array {
    $i = 0;

    do {
      // getAccount and getBillingInfo both throw the same error
      // and it's difficult to distinguish between the two. As
      // such, execute them separately.
      try {
        $this->recurlyClient->getAccount(("code-{$account_code}--$i"));
      }
      catch (NotFound $e) {
        return [
          'delta' => $i,
          'account_action' => 'create',
        ];
      }
      catch (\Exception $e) {
        throw new HardDeclineException('There was an issue adding your payment method. Please try again.');
      }

      try {
        $this->recurlyClient->getBillingInfo("code-{$account_code}--$i");
      }
      catch (NotFound $e) {
        // Account found, but without billing info.
        return [
          'delta' => $i,
          'account_action' => 'update',
        ];
      }
      catch (\Exception $e) {
        throw new HardDeclineException('There was an issue adding your payment method. Please try again.');
      }

      // An account was found for the given account code and
      // it already has billing info set.
      $i++;
      continue;

    } while (!isset($nonplan_target_delta));
  }

  /**
   * {@inheritDoc}
   */
  public function getPlanProductVariations(): array {

    $plan_product_variations = [];

    $product_variation_bundles = $this->entityTypeBundleInfo
      ->getBundleInfo('commerce_product_variation');

    foreach ($product_variation_bundles as $bundle_name => $bundle_info) {
      $bundle_fields = $this->entityFieldManager
        ->getFieldDefinitions(
          'commerce_product_variation',
          $bundle_name
        );

      // Our RecurlyPlanVariation trait attaches this field.
      if (!array_key_exists('plan_code', $bundle_fields)) {
        continue;
      }

      $plan_product_variations[] = $bundle_name;
    }

    return $plan_product_variations;
  }

}
