<?php

namespace Drupal\commerce_recurly\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Base class for Recurly Webhook Events.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyWebhookEventBase extends Event implements RecurlyWebhookEventInterface {

  /**
   * The push notification associated with the event.
   *
   * @var \Recurly_PushNotification
   */
  protected $notification;

  /**
   * The type of notification.
   *
   * @var string
   */
  protected $type;

  /**
   * The associated subdomain.
   *
   * @var ?string
   */
  protected $subdomain;

  /**
   * RecurlySuccessfulPaymentEvent constructor.
   *
   * @param \Recurly_PushNotification $notification
   *   Recurly Push Notification object provided by the webhook.
   * @param ?string $subdomain
   *   The associated Recurly subdomain.
   */
  public function __construct(\Recurly_PushNotification $notification, ?string $subdomain) {
    $this->notification = $notification;
    $this->type = $notification->type;
    $this->subdomain = $subdomain;
  }

  /**
   * {@inheritDoc}
   */
  public function getSubDomain(): ?string {
    return $this->subdomain;
  }

  /**
   * {@inheritDoc}
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * {@inheritDoc}
   */
  public function getNotification(): \Recurly_PushNotification {
    return $this->notification;
  }

}
