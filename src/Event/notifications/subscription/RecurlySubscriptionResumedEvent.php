<?php

namespace Drupal\commerce_recurly\Event\notifications\subscription;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Subscription Resumed notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlySubscriptionResumedEvent extends RecurlyWebhookEventBase {

}
