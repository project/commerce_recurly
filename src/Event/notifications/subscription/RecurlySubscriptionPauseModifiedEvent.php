<?php

namespace Drupal\commerce_recurly\Event\notifications\subscription;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Subscription Pause Modified notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlySubscriptionPauseModifiedEvent extends RecurlyWebhookEventBase {

}
