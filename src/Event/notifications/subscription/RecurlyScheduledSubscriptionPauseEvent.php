<?php

namespace Drupal\commerce_recurly\Event\notifications\subscription;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Schedule Subscription Pause notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyScheduledSubscriptionPauseEvent extends RecurlyWebhookEventBase {

}
