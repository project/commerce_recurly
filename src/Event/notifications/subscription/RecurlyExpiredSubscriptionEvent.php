<?php

namespace Drupal\commerce_recurly\Event\notifications\subscription;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Expired Subscription notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyExpiredSubscriptionEvent extends RecurlyWebhookEventBase {

}
