<?php

namespace Drupal\commerce_recurly\Event\notifications\subscription;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Paused Subscription Renewal notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyPausedSubscriptionRenewalEvent extends RecurlyWebhookEventBase {

}
