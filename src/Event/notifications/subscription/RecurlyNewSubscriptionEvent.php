<?php

namespace Drupal\commerce_recurly\Event\notifications\subscription;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for New Subscription notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyNewSubscriptionEvent extends RecurlyWebhookEventBase {

}
