<?php

namespace Drupal\commerce_recurly\Event\notifications\subscription;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Renewed Subscription notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyRenewedSubscriptionEvent extends RecurlyWebhookEventBase {

}
