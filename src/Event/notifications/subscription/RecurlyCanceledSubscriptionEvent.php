<?php

namespace Drupal\commerce_recurly\Event\notifications\subscription;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Canceled Subscription notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyCanceledSubscriptionEvent extends RecurlyWebhookEventBase {

}
