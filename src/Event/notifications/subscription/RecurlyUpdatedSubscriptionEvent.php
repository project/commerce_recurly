<?php

namespace Drupal\commerce_recurly\Event\notifications\subscription;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Updated Subscription notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyUpdatedSubscriptionEvent extends RecurlyWebhookEventBase {

}
