<?php

namespace Drupal\commerce_recurly\Event\notifications\payment;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Successful Payment notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlySuccessfulPaymentEvent extends RecurlyWebhookEventBase {

}
