<?php

namespace Drupal\commerce_recurly\Event\notifications\account;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Billing Info Update notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyBillingInfoUpdatedEvent extends RecurlyWebhookEventBase {

}
