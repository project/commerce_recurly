<?php

namespace Drupal\commerce_recurly\Event\notifications\account;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Canceled [sic] Account notifications.
 *
 * "Canceled" typo on purpose due to associated type
 * defined in \Recurly_PushNotification.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyCanceledAccountEvent extends RecurlyWebhookEventBase {

}
