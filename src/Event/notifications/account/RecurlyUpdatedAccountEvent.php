<?php

namespace Drupal\commerce_recurly\Event\notifications\account;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Updated Account notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyUpdatedAccountEvent extends RecurlyWebhookEventBase {

}
