<?php

namespace Drupal\commerce_recurly\Event\notifications\account;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for Billing Info Update Failed notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyBillingInfoUpdateFailedEvent extends RecurlyWebhookEventBase {

}
