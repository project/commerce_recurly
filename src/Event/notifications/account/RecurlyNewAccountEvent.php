<?php

namespace Drupal\commerce_recurly\Event\notifications\account;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for New Account notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyNewAccountEvent extends RecurlyWebhookEventBase {

}
