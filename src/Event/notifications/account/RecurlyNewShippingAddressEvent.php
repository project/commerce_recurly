<?php

namespace Drupal\commerce_recurly\Event\notifications\account;

use Drupal\commerce_recurly\Event\RecurlyWebhookEventBase;

/**
 * Event fired for New Shipping Address notifications.
 *
 * @package Drupal\commerce_recurly\Event
 */
class RecurlyNewShippingAddressEvent extends RecurlyWebhookEventBase {

}
