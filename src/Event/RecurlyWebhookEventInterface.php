<?php

namespace Drupal\commerce_recurly\Event;

/**
 * An interface for Recurly Webhook Events.
 *
 * @package Drupal\commerce_recurly\Event
 */
interface RecurlyWebhookEventInterface {

  /**
   * Gets the subdomain.
   *
   * @return ?string
   *   The associated subdomain
   */
  public function getSubDomain(): ?string;

  /**
   * Gets the type provided by the notification.
   *
   * @return string
   *   The notification type.
   */
  public function getType(): string;

  /**
   * Gets the associated push notification.
   *
   * @return \Recurly_PushNotification
   *   The notification associated with the event.
   */
  public function getNotification(): \Recurly_PushNotification;

}
