<?php

namespace Drupal\commerce_recurly\PluginForm\Recurly;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\commerce_recurly\Callback\PreRender;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to add credit card payment methods.
 *
 * This is used when adding a new credit card payment method via
 * Drupal\commerce_recurly\Plugin\Commerce\PaymentGateway\Recurly.
 *
 * @package Drupal\commerce_recurly\PluginForm\Recurly
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    // Set our key to settings array.
    /** @var \Drupal\commerce_recurly\Plugin\Commerce\PaymentGateway\RecurlyInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $payment_method_owner = $payment_method->getOwner();

    // Recurly token.
    $element['field_recurly_token'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'token'],
    ];

    $element['recurly_name'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'recurly-name',
      ],
    ];

    $element['recurly_name']['first_name'] = [
      '#type' => 'textfield',
      '#title' => 'First Name',
      '#attributes' => ['data-recurly' => 'first_name'],
    ];

    $element['recurly_name']['last_name'] = [
      '#type' => 'textfield',
      '#title' => 'Last Name',
      '#attributes' => ['data-recurly' => 'last_name'],
    ];

    $element['field_recurly_card'] = [
      '#type' => 'markup',
      '#markup' => '<div data-recurly="card"></div>',
    ];

    // Recurly token
    // Populated by recurly.js.
    $element['field_recurly_token'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'token'],
    ];

    $element['field_order_id'] = [
      '#type' => 'hidden',
      '#default_value' => \Drupal::routeMatch()->getParameter('commerce_order')->id(),
    ];

    $element['field_payment_method_owner_uid'] = [
      '#type' => 'hidden',
      '#default_value' => $payment_method_owner->id(),
    ];

    // Pass info to js.
    $data = [
      'public_key' => $payment_gateway_plugin->getPublicKey(),
    ];

    $element['#attached']['drupalSettings']['commerce_recurly'] = json_encode($data);

    $element['#attached']['library'][] = 'commerce_recurly/recurlyjs';
    $element['#attached']['library'][] = 'commerce_recurly/checkout';

    // Swiped from Stripe.
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($this->entity);
    $cacheability->setCacheMaxAge(0);
    $cacheability->applyTo($element);
    return $element;

  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // The JS library performs its own validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    if ($email = $form_state->getValue(['contact_information', 'email'])) {
      $email_parents = array_merge($element['#parents'], ['email']);
      $form_state->setValue($email_parents, $email);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    if (isset($form['billing_information'])) {
      $form['billing_information']['#after_build'][] = [
        get_class($this),
        'addAddressAttributes',
      ];
    }

    return $form;
  }

  /**
   * Adds our hidden data-recurly fields to the form.
   *
   * @param $element
   *
   * @todo: Would be cleaner to scrap this and add the attributes to the elements on the rendered billing profile.
   *
   */
  public static function addRecurlyHiddenFields($element) {
    // Name fields
    $element['field_first_name'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'first_name'],
    ];

    $element['field_last_name'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'last_name'],
    ];

    // Address fields
    $element['field_address1'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'address1'],
    ];

    $element['field_address2'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'address2'],
    ];

    $element['field_city'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'city'],
    ];

    $element['field_state'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'state'],
    ];

    $element['field_postal_code'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'postal_code'],
    ];

    $element['field_country'] = [
      '#type' => 'hidden',
      '#attributes' => ['data-recurly' => 'country'],
    ];

    return $element;
  }

  /**
   * Element #after_build callback: adds "data-recurly" to address properties.
   *
   * When adding/editing an address, these fields are provided by the
   * addressfield widget. As such, adding the 'data-recurly' attribute
   * allows recurly.js to pick up those values.
   *
   * If the user is re-using an existing billing address then these will not
   * be present, in which case we add hidden fields for all data-recurly
   * elements. These will be populated on form submit based on the rendered
   * profile address.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The modified form element.
   */
  public static function addAddressAttributes(array $element, FormStateInterface $form_state) {
    if (isset($element['address'])) {
      $element['address']['widget'][0]['address']['given_name']['#attributes']['data-recurly'] = 'first_name';
      $element['address']['widget'][0]['address']['family_name']['#attributes']['data-recurly'] = 'last_name';
      $element['address']['widget'][0]['address']['address_line1']['#attributes']['data-recurly'] = 'address1';
      $element['address']['widget'][0]['address']['address_line2']['#attributes']['data-recurly'] = 'address2';
      $element['address']['widget'][0]['address']['locality']['#attributes']['data-recurly'] = 'city';
      $element['address']['widget'][0]['address']['administrative_area']['#attributes']['data-recurly'] = 'state';
      $element['address']['widget'][0]['address']['postal_code']['#attributes']['data-recurly'] = 'postal_code';
      // Country code is a sub-element and needs another callback.
      $element['address']['widget'][0]['address']['country_code']['#pre_render'][] = [
        PreRender::class,
        'addCountryCodeAttributes',
      ];
    }
    else {
      $element = self::addRecurlyHiddenFields($element);
    }

    return $element;
  }

  /**
   * Element #pre_render callback: adds "data-recurly" to the country_code.
   *
   * This ensures data-recurly is on the hidden or select element for
   * the country code, so that it is properly passed to Stripe.
   *
   * @param array $element
   *   The form element.
   *
   * @return array
   *   The modified form element.
   */
  public static function addCountryCodeAttributes(array $element) {
    $element['country_code']['#attributes']['data-recurly'] = 'country';
    return $element;
  }

}
