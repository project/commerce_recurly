<?php

namespace Drupal\commerce_recurly\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Recurly configuration settings form.
 */
class CommerceRecurlyCustomFieldsSettingsForm extends CommerceRecurlyConfigFormBase {

  protected $recurly_gateways = [];


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_recurly_custom_fields_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_recurly.settings'];
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['overview'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Overview'),
    ];

    $form['overview']['desc'] = [
      '#markup' => $this->t('
        <p>Recurly allows the creation of <a href="https://docs.recurly.com/docs/custom-fields">Custom Fields</a> on Customer Accounts, Subscriptions, and Items. Fields added below will be set on the given object/custom field when appropriate.</p>
        '),
    ];

    $recurly_gateways_options = [];
    $default_gateway = NULL;

    $payment_gateway_manager = $this->entityTypeManager
      ->getStorage('commerce_payment_gateway');

    $payment_gateways = $payment_gateway_manager
      ->loadMultiple();

    foreach ($payment_gateways as $gateway) {
      if ($gateway->getPluginId() !== 'recurly') {
        continue;
      }

      // Helps with validation based on radio selection.
      $this->recurly_gateways[$gateway->id()] = $gateway;

      // Just used for the radio options.
      $recurly_gateways_options[$gateway->id()] = $gateway->label();

      if (!$default_gateway) {
        $default_gateway = $gateway->id();
      }
    }

    if (empty($this->recurly_gateways)) {
      $form['overview']['validation_gateway'] = [
        '#type' => 'markup',
        '#title' => 'asdfiluaksyjf',
        '#markup' => $this->t('You must add at least one Recurly <a href="/admin/commerce/config/payment-gateways">payment gateway</a> to manage this configuration. If you previously configured custom field information here, it has not been lost but cannot be edited until a payment gateway exists for the purposes of validation.'),
      ];


      unset($form['actions']);
      return $form;
    }

    $form['overview']['validation_gateway'] = [
      '#type' => 'radios',
      '#title' => $this->t('Custom Fields Validation Gateway'),
      '#description' => $this->t('The API keys defined on the selected gateway will be used to validate that the custom fields configured below are valid.'),
      '#options' => $recurly_gateways_options,
      '#default_value' => $default_gateway,
    ];

    $form['#tree'] = TRUE;

    $this->buildSection('Account', $form, $form_state);

    //    $this->buildSection('Subscription', $form, $form_state);
    $form['subscription'] = [
      '#type' => 'details',
      '#title' => $this->t("Subscription Fields"),
      '#description' => $this->t('Handling not yet configured.'),
      '#open' => FALSE,
    ];

    //    $this->buildSection('Item', $form, $form_state);
    $form['item'] = [
      '#type' => 'details',
      '#title' => $this->t("Item Fields"),
      '#description' => $this->t('Handling not yet configured.'),
      '#open' => FALSE,
    ];

    return $form;
  }

  public function buildSection($section_name, &$form, &$form_state) {
    $section_id = str_replace(' ', '_', strtolower($section_name));
    $section_id_kebab = str_replace('_', '-', $section_id);

    // Add form elements to collect default account information.
    $form[$section_id] = [
      '#type' => 'details',
      '#title' => $this->t("$section_name Fields"),
      '#open' => TRUE,
      '#attributes' => [
        'id' => $section_id_kebab,
      ],
    ];

    // Set these vals based on config unless they are falsey,
    // in which case set them to empty arrays.
    $custom_fields_config = $this->config('commerce_recurly.settings')
      ->get('custom_fields') ?: [];
    $this_fields_config =
      isset($custom_fields_config["{$section_id}_fields"]) ?
        $custom_fields_config["{$section_id}_fields"] :
        [];

    // Set data into indexed array to match it
    // when looping over $i below.
    $this_fields_config_indexed = [];
    foreach ($this_fields_config as $field_id => $field_pattern) {
      $this_fields_config_indexed[] = [
        'field_id' => $field_id,
        'field_pattern' => $field_pattern,
      ];
    }

    // Gather the number of names in the form already.
    $section_count = $form_state->get("{$section_id}_count");

    // If this hasn't been set then use set it based on
    // the number of values saved to config.
    if ($section_count === NULL) {
      $section_count = count($this_fields_config_indexed);
      $form_state->set("{$section_id}_count", $section_count);
    }

    // Generate fields based on $section_count and stored config
    for ($i = 0; $i < $section_count; $i++) {
      $i_plus = $i + 1;

      $form[$section_id][$i] = [
        '#type' => 'fieldset',
        '#title' => $this->t("Field $i_plus."),
      ];

      $form[$section_id][$i]['field_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Custom Field ID'),
        '#description' => $this->t('The ID of the custom field as it exists in Recurly.'),
        '#default_value' =>
          (isset($this_fields_config_indexed[$i]) && isset($this_fields_config_indexed[$i]['field_id'])) ?
            $this_fields_config_indexed[$i]['field_id'] :
            '',
        '#required' => TRUE,
      ];

      $form[$section_id][$i]['field_pattern'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Replacement Pattern'),
        '#description' => $this->t('The pattern used to populate the custom field.'),
        '#element_validate' => ['token_element_validate'],
        '#after_build' => ['token_element_validate'],
        '#token_types' => ['commerce_order'],
        '#default_value' =>
          (isset($this_fields_config_indexed[$i]) && isset($this_fields_config_indexed[$i]['field_pattern'])) ?
            $this_fields_config_indexed[$i]['field_pattern'] :
            '',
        '#required' => TRUE,
      ];

      // Show the token help relevant to this pattern type.
      $form[$section_id][$i]['field_token_selector'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['commerce_order'],
        '#global_types' => FALSE,
      ];
    }

    $form[$section_id]['actions'] = [
      '#type' => 'actions',
    ];

    $form[$section_id]['actions']['add_name'] = [
      '#type' => 'submit',
      '#name' => "add-$section_id_kebab",
      '#value' => $this->t('Add Item'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addRemoveCallback',
        'wrapper' => $section_id_kebab,
      ],
    ];

    // If there is more than one name, add the remove button.
    if ($section_count > 0) {
      $form[$section_id]['actions']['remove_name'] = [
        '#type' => 'submit',
        '#name' => "remove-$section_id_kebab",
        '#value' => $this->t('Remove Last Item'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addRemoveCallback',
          'wrapper' => $section_id_kebab,
        ],
      ];
    }

    return $form;
  }

  /**
   * Gets the key of the trigger's parent section.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function getTriggerSectionKey(FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $trigger_parents = $trigger['#array_parents'];
    return reset($trigger_parents);
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addRemoveCallback(array &$form, FormStateInterface $form_state) {
    return $form[$this->getTriggerSectionKey($form_state)];
  }


  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $this->addRemoveHandler('add', $form_state);
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $this->addRemoveHandler('remove', $form_state);
  }

  /**
   * Add/remove handler.
   *
   * @param $variant
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addRemoveHandler($variant, FormStateInterface $form_state) {
    $trigger_section_name = $this->getTriggerSectionKey($form_state);
    $count_key = "{$trigger_section_name}_count";
    $section_count = $form_state->get($count_key);

    switch ($variant) {
      case 'add':
        $section_count++;
        break;

      case 'remove':
        if ($section_count > 0) {
          $section_count--;
        }
        break;

    }

    // Need to cast this to a string or else a value of 0
    // will be interpreted as false, leading to the relevant
    // count being incorrectly set to null. If this happens
    // then the number of fieldsets rendered will be equal to
    // the number of values stored in the related config set.
    $form_state->set($count_key, (string) $section_count);

    // Since our buildForm() method relies on the value of the section's count
    // to generate form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $validation_gateway_id = $form_state
      ->getValue('overview')['validation_gateway'];
    //
    $validation_gateway = $this->recurly_gateways[$validation_gateway_id];
    $gateway_conf = $validation_gateway->getPluginConfiguration();
    $private_key = $gateway_conf['private_key'];

    $recurly_client = $this->recurlyClient->init($private_key);

    $recurly_object_types = [
      'account',
      'subscription',
      'item',
    ];

    foreach ($recurly_object_types as $type) {
      // Similar logic also used in Recurly.php gateway logic.
      // Should probably refactor to a shared implementation.
      $custom_field_definitions = $recurly_client->listCustomFieldDefinitions(['related_type' => 'account']);

      $custom_field_definitions_by_name = [];
      foreach ($custom_field_definitions as $k => $custom_field) {
        $custom_field_definitions_by_name[$custom_field->getName()] = $custom_field;
      }

      foreach ($form_state->getValue($type, []) as $k => $v) {
        $trigger = $form_state->getTriggeringElement();

        // Don't want to hit this when the element is being removed,
        // as it may be empty or invalid.
        if (strpos($trigger['#name'], 'remove') !== FALSE) {
          continue;
        }

        // Ignore actions elements
        if (!is_numeric($k)) {
          continue;
        }

        // So we don't throw this particular error on empty fields.
        if (empty($v['field_id']) || empty($v['field_pattern'])) {
          continue;
        }

        // Indicates that the remote custom field exists
        if (array_key_exists($v['field_id'], $custom_field_definitions_by_name)) {
          continue;
        }

        // Make our error message more readable
        $type_str = $type;
        if ($type_str === 'account') {
          $type_str = 'customer account';
        }
        $type_str = ucwords($type_str);

        // add validation error
        $form_state->setErrorByName("$type][$k", t("A custom field named <i>{$v['field_id']}</i> doesn't exist on <i>$type_str</i> objects in the Recurly Account related to the selected validation gateway."));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Custom field data is stored in a single array
    // keyed by recurly entity type.
    $custom_fields_data = [];

    $custom_field_section_ids = [
      'account',
      'subscription',
      'item',
    ];

    foreach ($custom_field_section_ids as $section_name) {
      // Use the recurly entity name to set an array key that's
      // slightly friendlier when fetching the data.
      $section_id = "{$section_name}_fields";

      $custom_fields_data[$section_id] = [];

      if (!$values = $form_state->getValue($section_name)) {
        continue;
      }

      foreach ($values as $k => $field_info) {
        // Ignore Actions
        if (!is_int($k)) {
          continue;
        }

        // Final check so that we don't store empty data
        if (empty($field_info['field_id']) || empty($field_info['field_pattern'])) {
          continue;
        }

        $field_id = $field_info['field_id'];
        $field_pattern = $field_info['field_pattern'];

        $custom_fields_data[$section_id][$field_id] = $field_pattern;
      }
    }

    $this->config('commerce_recurly.settings')
      ->set('custom_fields', $custom_fields_data)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
