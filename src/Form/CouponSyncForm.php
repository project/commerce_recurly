<?php

namespace Drupal\commerce_recurly\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form that can sync recurly coupons to commerce promotions..
 */
class CouponSyncForm extends FormBase {

  /**
   * The coupon sync service.
   *
   * @var \Drupal\commerce_recurly\CouponSync
   */
  private $couponSync;

  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->couponSync = $container->get('commerce_recurly.coupon_sync');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_recurly_coupon_sync_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['message'] = [
      '#markup' => $this->t('This form will sync down all recurly coupons as Commerce promotions + coupons.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync Coupons From Recurly'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->couponSync->syncRecurlyCouponsAsPromotions();
  }

}
