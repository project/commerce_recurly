<?php


namespace Drupal\commerce_recurly\Traits;


use Recurly\RecurlyError;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Recurly\Errors\NotFound;

trait AccountTrait {

  /**
   * {@inheritDoc}
   */
  public function getAccountCodePattern(string $pattern_key): ?string {
    return $this->configuration['account_code_patterns'][$pattern_key];
  }

  /**
   * {@inheritDoc}
   */
  public function getAccountCodePatterns(): ?array {
    return $this->configuration['account_code_patterns'];
  }

  /**
   * Returns a nonplan account code given a code and a delta.
   *
   * @param $account_code
   * @param $delta
   *
   * @return string
   */
  protected function constructNonplanAccountCode($account_code, $delta) {
    return "$account_code--$delta";
  }

  /**
   * Determine proper account code pattern based on the order's items.
   *
   * @param $order
   *   The order for which we are determining the account code pattern.
   *
   * @return string
   *   The account code pattern type.
   * @todo: Need to generalize this so it can be used when adding payment method.
   */
  public function getAccountCodePatternType($order) {
    $plan_product_variations = $this->commerceRecurlyUtils->getPlanProductVariations();

    foreach ($order->getItems() as $order_item) {
      $item_product_variation_type = $order_item
        ->getPurchasedEntity()
        ->bundle();

      if (in_array($item_product_variation_type, $plan_product_variations)) {
        return 'plan';
      }
    }

    return 'nonplan';
  }

  public function getAccountCodeByOrder(string $account_code_pattern_key, OrderInterface $order) {
    $plan_account_code_pattern = $this->getAccountCodePattern($account_code_pattern_key);
    return $this->token
      ->replace($plan_account_code_pattern, ['commerce_order' => $order]);
  }


  /**
   * Get the Recurly "plan" account for the owner of an order.
   *
   * Ideally this logic would be independent of a commerce order,
   * but at the account code config only allows tokens that are
   * based on a commerce_order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order whose owner corresponds to the Recurly account.
   * @param bool $create_missing_account
   *   Whether or not a new account should be created if an
   *   existing one is not found.
   *
   * @return \Recurly\Resources\Account
   */
  public function getPlanAccountByOrder(OrderInterface $order, bool $create_missing_account = FALSE) {
    $plan_account_code = $this->getAccountCodeByOrder('plan', $order);
    
    try {
      return $this->recurlyClientInstance
        ->getAccount("code-{$plan_account_code}");
    }
    catch (NotFound $e) {
      if (!$create_missing_account) {
        throw new $e;
      }

      // Just means we need to create the account.
      $account_data = [
        'code' => $plan_account_code,
        'email' => $order->getEmail(),
        //        'first_name' => $nonplan_payment_method_account->getFirstName(),
        //        'last_name' => $nonplan_payment_method_account->getLastName(),
      ];

      $custom_fields = $this->generateRecurlyCustomFieldData($order);
      if (!empty($custom_fields)) {
        $account_data['custom_fields'] = $custom_fields;
      }

      return $this->recurlyClientInstance->createAccount($account_data);
    }
    catch (RecurlyError $e) {
      throw new HardDeclineException($e->getMessage());
    }
    catch (\Exception $e) {
      throw new HardDeclineException($e->getMessage());
    }
  }

}
