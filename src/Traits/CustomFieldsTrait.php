<?php


namespace Drupal\commerce_recurly\Traits;


trait CustomFieldsTrait {

  /**
   * Generates custom field data to attach to a Recurly object.
   *
   * @param null $order
   *   The order being used for token replacement.
   *
   * @return array
   *   Array of custom field data.
   */
  protected function generateRecurlyCustomFieldData($order = NULL) {
    $custom_fields = [];

    if (isset($this->configCustomFields['account_fields'])) {
      if ($account_custom_fields = $this->configCustomFields['account_fields']) {
        $custom_field_definitions = $this->recurlyClientInstance->listCustomFieldDefinitions(['related_type' => 'account']);
        $custom_field_definitions_by_name = [];

        foreach ($custom_field_definitions as $k => $custom_field) {
          $custom_field_definitions_by_name[$custom_field->getName()] = $custom_field;
        }


        foreach ($account_custom_fields as $field_id => $field_pattern) {
          if (!array_key_exists($field_id, $custom_field_definitions_by_name)) {
            continue;
          }

          // - token decode pattern
          $field_value = $this->token
            ->replace($field_pattern, ['commerce_order' => $order]);

          $custom_fields[] = [
            'name' => $field_id,
            'value' => $field_value,
          ];
        }
        $account_data['custom_fields'] = $custom_fields;
      }
    }

    return $custom_fields;
  }

}
