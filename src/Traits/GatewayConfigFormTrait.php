<?php

namespace Drupal\commerce_recurly\Traits;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 *
 */
trait GatewayConfigFormTrait {

  /**
   * Helps to persist checkbox state thru ajax updates.
   *
   * @var bool
   */
  protected $checkboxState;

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $config = [
      'subdomain' => '',
      'private_key' => '',
      'public_key' => '',
      'account_code_patterns' => [
        'plan' => '',
        'nonplan' => '',
      ],
    ];

    return $config + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $this->commerceRecurlyUtils->getPlanProductVariations();

    $recurly_module_config_url =
      Url::fromRoute('recurly.settings_form')
        ->toString();

    $form['mode']['#description'] = $this->t('Note that this has no effect on API calls and exists for developer use only. Recurly accounts can only be configured as one of <a href="https://support.recurly.com/hc/en-us/articles/360027273912-What-is-the-difference-between-Sandbox-Development-and-Production-sites-">Sandbox, Developer, or Production</a> -- never multiple. If you need to handle separate test/live environments then you will need to set up separate accounts for each environment as per <a href="https://support.recurly.com/hc/en-us/articles/360026665332-How-can-I-open-a-new-sandbox-site-for-testing-">Recurly\'s documentation</a>. Modules such as <a href="https://www.drupal.org/project/config_split">Config Split</a> are recommended for this scenario. All payment gateways will use the API creds configured in the <a href="@recurly_module_config_url">Recurly Module Settings</a>.',
      [
        '@recurly_module_config_url' => $recurly_module_config_url,
      ]);

    $form['#attached']['library'][] = 'commerce_recurly/admin';

    // Not applicable right now, but added in anticipation of
    // potential eventual removal of dependency on Recurly module.
    $form['recurly_module_notice'] = [
      '#type' => 'markup',
      '#markup' => $this->t("If the <a href='https://www.drupal.org/project/recurly'>Recurly module</a> is enabled then you may use its configuration to populate these API details."),
    ];

    if ($this->recurlyModuleExists) {
      // This was originally designed as a checkbox, but for the sake
      // of simplicity it's been hidden instead. This makes it easier
      // to reliably access the proper recurly account from outside of
      // commerce flows. Associated fields in $this->renderDependentElements()
      // have also been made hidden, but should be switched back to
      // 'textfields' if this checkbox is reimplemented as such.
      $form['use_recurly_module_creds'] = [
        // '#type' => 'checkbox',
        '#type' => 'hidden',
        '#title' => $this
          ->t(
            "Use API credentials configured in <a href='@url'>Recurly Module Settings</a> instead of setting them here.",
            [
              '@url' => $recurly_module_config_url,
            ]),
        // '#default_value' => $this->configuration['use_recurly_module_creds']
        // ?? 0,
        '#default_value' => 1,
        '#value' => 1,
        '#description' => $this->t('If this is checked then any previously entered API details will be overridden by those in the above configuration. <strong>Important: This is left around in anticipation of potential future capabilities. Unchecking this box may cause general instability.</strong>'),
        '#attributes' => [
          'disabled' => ['disabled'],
          'checked' => ['checked'],
        ],
      ];

      unset($form['recurly_module_notice']);
    }

    $this->renderDependentElements($form);
    $this->renderAccountCodePatternsElements($form);

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    if ((int) $values['use_recurly_module_creds'] === 1) {
      // Temporarily store all form errors.
      $form_errors = $form_state->getErrors();

      // Clear the form errors.
      $form_state->clearErrors();

      $dependent_elements = [
        'subdomain',
        'private_key',
        'public_key',
      ];

      foreach ($dependent_elements as $element_name) {
        // This is ignored by validation but set for sanity.
        $form['dependent_elements'][$element_name]['#required'] = FALSE;
        $form['dependent_elements'][$element_name]['#required_but_empty'] = FALSE;

        $parents = $form['#parents'];
        $parents_str = implode('][', $parents);

        // Loop over existing error messages in search of
        // validation issues for empty dependent elements.
        foreach ($form_errors as $error_key => $error_message) {
          if ($error_key !== "$parents_str][dependent_elements][$element_name") {
            continue;
          }

          if (strpos($error_message->getUntranslatedString(), 'is required') === FALSE) {
            continue;
          }

          unset($form_errors[$error_key]);
        }
      }

      // Loop through and re-apply remaining error messages.
      foreach ($form_errors as $name => $error_message) {
        $form_state->setErrorByName($name, $error_message);
      }

      if (empty($this->recurlyModuleConfig->get('recurly_private_api_key'))) {
        $recurly_config_error_detected = TRUE;
        $form_state->setErrorByName('configuration][recurly_js_checkout][dependent_elements][private_key', t('Your Recurly module configuration is missing a private key.'));
      }

      if (empty($this->recurlyModuleConfig->get('recurly_public_key'))) {
        $recurly_config_error_detected = TRUE;
        $form_state->setErrorByName('configuration][recurly_js_checkout][dependent_elements][public_key', t('Your Recurly module configuration is missing a public key.'));
      }

      if (empty($this->recurlyModuleConfig->get('recurly_subdomain'))) {
        $recurly_config_error_detected = TRUE;
        $form_state->setErrorByName('configuration][recurly_js_checkout][dependent_elements][subdomain', t('Your Recurly module configuration is missing a subdomain.'));
      }

      if (isset($recurly_config_error_detected)) {
        $form_state->setErrorByName('use_recurly_module_creds', t("Unable to use API Details from your <a href='http://dywm8.lndo.site/admin/config/services/recurly'>Recurly Module Configuration</a>. Please remedy the above errors and then try again."));
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $this->configuration['use_recurly_module_creds'] = $values['use_recurly_module_creds'];

    // Set API Details.
    $this->configuration['subdomain'] =
      ((int) $values['use_recurly_module_creds'] === 1) ?
        $this->recurlyModuleConfig->get('recurly_subdomain') :
        $values['dependent_elements']['subdomain'];
    $this->configuration['private_key'] =
      ((int) $values['use_recurly_module_creds'] === 1) ?
        $this->recurlyModuleConfig->get('recurly_private_api_key') :
        $values['dependent_elements']['private_key'];
    $this->configuration['public_key'] =
      ((int) $values['use_recurly_module_creds'] === 1) ?
        $this->recurlyModuleConfig->get('recurly_public_key') :
        $values['dependent_elements']['public_key'];
    $this->configuration['subdomain'] =
      ((int) $values['use_recurly_module_creds'] === 1) ?
        $this->recurlyModuleConfig->get('recurly_subdomain') :
        $values['dependent_elements']['subdomain'];

    // Set account ID patterns
    // Initialize as empty array to clear out potentially stale keys.
    $this->configuration['account_code_patterns'] = [];

    $this->configuration['account_code_patterns']['plan'] =
      $values['account_code_patterns']['plans_nonplans']['plans'];

    $this->configuration['account_code_patterns']['nonplan'] =
      $values['account_code_patterns']['plans_nonplans']['nonplans'];
  }

  /**
   * Adds the dependent API elements to the form.
   *
   * @param $form
   *   The form.
   */
  private function renderDependentElements(&$form) {
    // A container for elements that should respond to the
    // AJAX provided by ['use_recurly_module_creds'].
    $form['dependent_elements'] = [
      '#type' => 'container',
      '#hidden' => TRUE,
      '#attributes' => [
        'id' => 'dependent-elements',
      ],
    ];

    // Dependent elements should always be #required => true
    // to avoid values ever being allowed to be saved as empty
    // regardless of ['use_recurly_module_creds'] state.
    // Handling for shouldUseRecurlyConfig() is defined in
    // $this->validateConfigurationForm()
    //
    // 1/20/21 - These were all set as 'hidden' instead of
    // 'textfield' when the associated checkbox was removed.
    // If checkbox functionality is restored then these field's
    // types can simply be reverted.
    $form['dependent_elements']['subdomain'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Subdomain'),
      '#description' => $this->t('This is the Recurly subdomain.'),
      '#default_value' => $this->getSubdomain(),
      '#required' => TRUE,
    ];

    $form['dependent_elements']['private_key'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Private key'),
      '#description' => $this->t('This is the private key from Recurly.'),
      '#default_value' => $this->getPrivateKey(),
      '#required' => TRUE,
    ];

    $form['dependent_elements']['public_key'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Public key'),
      '#description' => $this->t('This is the public key from Recurly.'),
      '#default_value' => $this->getPublicKey(),
      '#required' => TRUE,
    ];

    foreach ($form['dependent_elements'] as $k => $element) {
      if (strpos($k, '#') !== FALSE) {
        continue;
      }

      $element['#hidden'] = !$this->shouldUseRecurlyModuleConfig();
      $element['#disabled'] = !$this->shouldUseRecurlyModuleConfig();
      $element['#required'] = $this->shouldUseRecurlyModuleConfig();
    }
  }

  /**
   * Adds Account Code Patterns elements to the form.
   *
   * @param $form
   *   The form
   */
  private function renderAccountCodePatternsElements(&$form) {
    // Details container for all patterns. If tabs worked
    // then this would be a tab.
    $form['account_code_patterns'] = [
      '#type' => 'details',
      '#title' => $this->t('Account Code Patterns'),
      '#description' => $this->t("<p>Recurly only allows a single billing method to be associated with any given customer account. To get around this limitation, Commerce Recurly defines two types of accounts.<p>

      <p>First, when a user creates a new commerce_payment_method during checkout, a <b>Nonplan Account</b> is created in Recurly whose Account Code uses the pattern defined below plus a delta (so <code>payment-method--[commerce_order:uid:target_id]--0</code>, <code>payment-method--[commerce_order:uid:target_id]--1</code>, etc) and the payment method is associated with that customer account.</p>

      <p>When the user then uses this commerce_payment_method to purchase a Product Variation that been marked as a plan, a new <b>Plan Account</b> is created on Recurly (or is fetched if it already exists). In Recurly, this account is set as a 'child' of the nonplan account associated with the payment method and is marked as 'bill to parent'. In this way, we essentially treat Nonplan Customers as pseudo credit card objects.</p>

      <p>That said, at the moment this means that if the user purchases another plan product at a later time with a different payment method, the Plan Account's parent will be updated accordingly. In this case, all future subscription charges for the Plan Account will be billed to this new payment method.</p>

      <p>Doublecheck your tokens. For instance, the User ID lives on <code>[commerce_order:uid:target_id]</code>, not on <code>[commerce_order:uid]</code>. Also note that payment methods can currently only be added during checkout since an Order is required for token replacement.</p>"),
      '#group' => 'account_code_patterns_tabs',
      '#open' => TRUE,
      '#required' => TRUE,
    ];

    // Vertical tabs do display as they should, unfortunately,
    // so we're dropping them all in a single group to at least
    // give them a styled details container.
    $form['account_code_patterns__vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#required' => TRUE,
      '#attributes' => [
        'id' => 'account-id-patterns-vertical--tabs',
      ],
    ];

    $this->renderAccountCodePatternsPlansNonplans($form);
  }

  /**
   * Adds Plan vs Nonplan Account Code Patterns elements to the form.
   *
   * @param $form
   *   The form
   */
  private function renderAccountCodePatternsPlansNonplans(&$form) {
    // "Tabs" container for product variation type patterns.
    $form['plans_nonplans__vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#required' => TRUE,
      '#attributes' => [
        'id' => 'plans-nonplans--vertical-tabs',
      ],
    ];

    // Details for for product variation types patterns.
    $form['account_code_patterns']['plans_nonplans'] = [
      '#type' => 'details',
      '#title' => $this->t('Plan + Nonplan Patterns'),
      '#required' => TRUE,
      '#group' => 'plans_nonplans__vertical_tabs',
    ];

    // Define our default/fallback sections and their dependent properties.
    $sections = [
      'plans' => [
        'title' => 'Plans',
        'description' => 'This pattern is used exclusively when purchasing Product Variant Types that are treated as plans.',
        'default_value' => $this->getAccountCodePattern('plan') ?: '',
        'required' => TRUE,
      ],
      'nonplans' => [
        'title' => 'Nonplans',
        'description' => 'This pattern is used exclusively when purchasing Product Variant Types that are not treated as plans.',
        'default_value' => $this->getAccountCodePattern('nonplan') ?: '',
        'required' => TRUE,
      ],
    ];

    $this->renderAccountCodePatternElement($form, 'plans_nonplans', $sections);
  }

  /**
   * Renders a set of elements that share a majority of their information.
   *
   * @param $form
   *   The form
   * @param $key
   *   Key to be used for grouping the elements
   * @param array $sections
   *   The sections to be rendered.
   *   $section = [
   *     'section_1_name' => [
   *       'title' => ...,
   *       'description' => ...,
   *       'default_value' => ...,
   *       'required' => ...
   *     ]
   *   ].
   */
  private function renderAccountCodePatternElement(&$form, $key, $sections) {
    // Build our default + fallback form elements.
    foreach ($sections as $section_name => $section_details) {
      $form['account_code_patterns'][$key][$section_name] = [
        '#type' => 'textfield',
        '#title' => $this->t($section_details['title'] . '  Account Code Pattern'),
        '#description' => $this->t($section_details['description']),
        '#default_value' => $section_details['default_value'],
        '#size' => 65,
        '#maxlength' => 1280,
        '#element_validate' => ['token_element_validate'],
        '#after_build' => ['token_element_validate'],
        '#token_types' => ['commerce_order'],
        '#min_tokens' => 1,
        '#required' => $section_details['required'],
      ];

      // Show the token help relevant to this pattern type.
      $form['account_code_patterns'][$key]["account_code_pattern_token_help__$section_name"] = [
        '#theme' => 'token_tree_link',
        '#token_types' => ['commerce_order'],
        // '#token_types' => ['commerce_order', 'commerce_payment'],
        '#global_types' => FALSE,
      ];
    }
  }

  /**
   * Determines whether or not we should be using
   * API data from the Recurly module's config
   * instead of setting it using this config form.
   *
   * @return bool
   */
  private function shouldUseRecurlyModuleConfig() {
    if (isset($this->checkboxState)) {
      if ($this->checkboxState === 0) {
        return FALSE;
      }

      return TRUE;
    }

    if (!$this->recurlyModuleExists) {
      return FALSE;
    }

    if (!isset($this->configuration['use_recurly_module_creds']) || $this->configuration['use_recurly_module_creds'] == 0) {
      return FALSE;
    }

    return TRUE;
  }

}
