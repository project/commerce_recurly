<?php

namespace Drupal\commerce_recurly\Traits;

trait ApiCredentialsTrait {

  /**
   * Initialize the recurly client
   */
  public function initClient() {
    if (!isset($this->recurlyClientInstance)) {
      $this->recurlyClientInstance = $this->recurlyClient->init($this->getPrivateKey());
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getSubdomain(): ?string {
    return $this->configuration['subdomain'];
  }

  /**
   * {@inheritDoc}
   */
  public function getPrivateKey(): ?string {
    return $this->configuration['private_key'];
  }

  /**
   * {@inheritDoc}
   */
  public function getPublicKey(): ?string {
    return $this->configuration['public_key'];
  }

}
