<?php

namespace Drupal\commerce_recurly;


use Recurly\Client;

/**
 * Service implementation of the Recurly Client.
 *
 * @package Drupal\commerce_recurly
 */
interface RecurlyClientInterface {

  /**
   * Initialize an instance of the Recurly Client.
   *
   * @param string $private_key
   *   The API private key.
   *
   * @return \Recurly\Client
   *   The recurly client instance.
   */
  public function init(string $private_key): Client;

  /**
   * Initialize a client using the stored private key.
   *
   * @return \Recurly\Client
   *   The recurly client instance.
   */
  public function initDefault(): Client;

}
