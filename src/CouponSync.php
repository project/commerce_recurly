<?php

namespace Drupal\commerce_recurly;

use Recurly\Pager;
use Drupal\commerce_promotion\Entity\CouponInterface;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Http\Discovery\Exception\NotFoundException;
use Recurly\Resources\Coupon;
use Recurly\Resources\UniqueCouponCode;

/**
 * Provides a way to sync recurly coupons down as commerce promotions/coupons.
 */
class CouponSync {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The recurly client.
   *
   * @var \Recurly\Client
   */
  private $client;

  /**
   * The variation entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $variationManager;

  /**
   * The promotion entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $promotionManager;

  /**
   * The coupon entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $couponManager;

  /**
   * A map between local product variations and remote recurly plan codes.
   *
   * @var array
   */
  protected $variationMap = [];

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * Whether there were errors when syncing.
   *
   * This is needed because setting messages with placeholders is
   * throwing FlashBag errors for some reason that doesn't make sense.
   *
   * @var bool
   */
  private $syncdWithErrors = FALSE;

  /**
   * Constructs a Coupons object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_recurly\RecurlyClientInterface $commerce_recurly_recurly_client
   *   The commerce_recurly.recurly_client service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RecurlyClientInterface $commerce_recurly_recurly_client, MessengerInterface $messenger, LoggerChannelInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->client = $commerce_recurly_recurly_client->initDefault();
    $this->logger = $logger;

    try {
      $this->variationManager = $this->entityTypeManager->getStorage('commerce_product_variation');
      $this->promotionManager = $this->entityTypeManager->getStorage('commerce_promotion');
      $this->couponManager = $this->entityTypeManager->getStorage('commerce_promotion_coupon');
    }
    catch (PluginException $e) {
      $this->messenger->addError($e->getMessage() . "... Maybe you need to enable commerce_promotion module?");
    }
  }

  /**
   * Syncs down all Recurly Coupons as Commerce Promotions.
   *
   * If a promotion already exist for the coupon, or coupons
   * on the promotion exist that match the recurly coupon,
   * the commerce promotion/coupon will not be generated or
   * updated.
   *
   * NOTE: It seems that there is something odd going on with
   * the Promotion::create() method in that the start_date
   * and end date appear as expected on the main promotion
   * listing page, but they do not appear when editing the
   * promotion itself.
   */
  public function syncRecurlyCouponsAsPromotions() {
    $recurly_coupons = $this->client->listCoupons();
    $query = $this->variationManager->getQuery();
    $query->condition('type', 'recurly_plan_variation');
    $recurly_plan_variation_ids = $query->execute();

    foreach ($recurly_plan_variation_ids as $vid) {
      $variation = $this->variationManager->load($vid);
      $uuid = $variation->get('uuid')->value;
      $recurly_plan_code = trim($variation->get('plan_code')->value);
      $this->variationMap[$recurly_plan_code] = [(int) $vid => $uuid];
    }

    /** @var \Recurly\Resources\Coupon $recurly_coupon */
    foreach ($recurly_coupons as $recurly_coupon) {
      // We should actually be storing the remote recurly coupon ID
      // on the promotions to ensure that we are updating the correct
      // ones. But this is to handle a case where an old coupon uses
      // the same code as a new one.
      if ($recurly_coupon->getState() === 'expired') {
        continue;
      }

      $promo_names = $this->getPromotionNames($recurly_coupon);
      $recurly_coupon_code = $recurly_coupon->getCode();

      $code_query = $this->promotionManager->getQuery();
      $code_query->condition('name', $recurly_coupon_code, 'ENDS_WITH');
      $results_code_query = $code_query->execute();

      $full_name_query = $this->promotionManager->getQuery();
      $full_name_query->condition('name', $promo_names['primary_name']);
      $results_full_name_query = $full_name_query->execute();

      try {
        if (!empty($results_full_name_query)
          || !empty($results_code_query)) {
          if (count($results_code_query) > 1
            || count($results_full_name_query) > 1) {
            $this->syncdWithErrors = TRUE;
            $this->logger->error("Multiple local promotions seem to exists for the Recurly Coupon: $recurly_coupon_code. Sync was skipped for this coupon.");
            continue;
          }

          $results_to_use = !empty($results_full_name_query)
            ? $results_full_name_query
            : $results_code_query;
          $pid = reset($results_to_use);

          /** @var PromotionInterface $promotion */
          $promotion = $this->promotionManager->load($pid);
          $this->syncStaleEndDate($recurly_coupon, $promotion)
            ->save();
        }
        else {
          $promotion = $this->createCommercePromotion($recurly_coupon);
        }
      }
      catch (NotFoundException $e) {
        continue;
      }

      /** @var PromotionInterface $promotion */
      $this->generateCommerceCoupons($recurly_coupon, $promotion);
    }

    if ($this->syncdWithErrors) {
      $this->messenger->addWarning('Coupons syncd, but there were some errors. Some coupons may have been skipped. Please check the site error logs for more information.');
    }
    else {
      $this->messenger->addStatus('Coupons syncd from Recurly.');
    }
  }

  /**
   * Get the primary and display names from the coupon to use on the promo.
   *
   * @param \Recurly\Resources\Coupon $recurly_coupon
   *
   * @return array
   *   The primary and display names.
   */
  protected function getPromotionNames(Coupon $recurly_coupon): array {
    $recurly_coupon_name = $recurly_coupon->getName();
    $recurly_coupon_code = $recurly_coupon->getCode();

    return [
      'primary_name' => "(Recurly) $recurly_coupon_name - $recurly_coupon_code",
      'display_name' => $recurly_coupon->getHostedPageDescription(),
    ];
  }

  /**
   * Create a commerce promotion for a recurly coupon.
   *
   * @param \Recurly\Resources\Coupon $recurly_coupon
   *   The recurly coupon.
   *
   * @return \Drupal\commerce_promotion\Entity\PromotionInterface
   *   The generated promotion.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createCommercePromotion(Coupon $recurly_coupon): PromotionInterface {
    $recurly_discount = $recurly_coupon->getDiscount();
    $promo_names = $this->getPromotionNames($recurly_coupon);
    $discount_type = $recurly_discount->getType();

    $offer_type_dictionary = [
      'fixed' => 'order_item_fixed_amount_off',
      'percent' => 'order_item_percentage_off',
      'free_trial' => 'order_item_percentage_off',
    ];

    // Set up the common metadata
    $promotion_metadata = [
      'name' => $promo_names['primary_name'],
      'display_name' => $promo_names['display_name'],
      'order_types' => ['recurly'],
      'stores' => [1],
      'offer' => [
        'target_plugin_id' => $offer_type_dictionary[$discount_type],
        'target_plugin_configuration' => [
          'display_inclusive' => FALSE,
        ],
      ],
      'usage_limit' => $recurly_coupon->getMaxRedemptions(),
      'usage_limit_customer' => $recurly_coupon->getMaxRedemptionsPerAccount(),
      'status' => $recurly_coupon->getState() === 'expired' ? 0 : 1,
    ];

    // Match the remote plans with local product variations
    foreach ($recurly_coupon->getPlans() as $plan) {
      $plan_code = $plan->getCode();
      if (!isset($this->variationMap[$plan_code])) {
        $this->syncdWithErrors = TRUE;
        $this->logger->error("Plan code does not exist in commerce: :plan_code. Skipping coupon :coupon_name.", [
          ':plan_code' => $plan_code,
          ':coupon_name' => $recurly_coupon->getName(),
        ]);
        throw new NotFoundException("Product not found for $plan_code");
      }

      $applicable_product_variations = $this->variationMap[$plan_code];
    }

    if (isset($applicable_product_variations)) {
      $promotion_metadata['offer']['target_plugin_configuration']['conditions'][] = [
        'plugin' => 'order_item_purchased_entity:commerce_product_variation',
        'configuration' => [
          'entities' => $applicable_product_variations,
        ],
      ];

      $promotion_metadata['conditions'][] = [
        'target_plugin_id' => 'order_purchased_entity:commerce_product_variation',
        'target_plugin_configuration' => [
          'entities' => $applicable_product_variations,
        ],
      ];
    }

    // Set the type/value of the discount
    switch ($discount_type) {
      case 'fixed':
        /** @var \Recurly\Resources\CouponDiscountPricing $currency */
        foreach ($recurly_discount->getCurrencies() as $currency) {
          $promotion_metadata['offer']['target_plugin_configuration']['amount'] = [
            'number' => $currency->getAmount(),
            'currency_code' => $currency->getCurrency(),
          ];
        }
        break;
      case 'percent':
        $promotion_metadata['offer']['target_plugin_configuration']['amount'] = $recurly_discount->getPercent() / 100;
        break;
      case 'free_trial':
        // Free trial is basically a 100% discount.
        $promotion_metadata['offer']['target_plugin_configuration']['amount'] = 1;
        break;
    }

    /** @var \Drupal\commerce_promotion\Entity\PromotionInterface $promotion */
    $promotion = $this->promotionManager->create($promotion_metadata);
    $this->syncDates($recurly_coupon, $promotion);

    $promotion->save();
    return $promotion;
  }

  /**
   * Convert a string a DrupalDateTime in the storage timezone.
   *
   * @param string $recurly_time
   *   The string time.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   The timezoned datetime.
   */
  protected function stringToDateTime(string $recurly_time): DrupalDateTime {
    $date = new DrupalDateTime($recurly_time);
    $date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    return $date;
  }

  /**
   * Sync start and end dates from recurly to local promo/coupon.
   *
   * @param \Recurly\Resources\Coupon $recurly_coupon
   *   The recurly coupon.
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface|\Drupal\commerce_promotion\Entity\CouponInterface $promotion_or_coupon
   *   The promo or coupon that matches the remote coupon.
   *
   * @return void
   */
  protected function syncDates(Coupon $recurly_coupon, PromotionInterface|CouponInterface $promotion_or_coupon): void {
    $start_date = $this->stringToDateTime($recurly_coupon->getCreatedAt());
    $promotion_or_coupon->setStartDate($start_date);

    $this->syncStaleEndDate($recurly_coupon, $promotion_or_coupon);
  }

  /**
   * Whether the recurly and local end dates match.
   *
   * @param \Recurly\Resources\Coupon $recurly_coupon
   *   The recurly coupon.
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface|\Drupal\commerce_promotion\Entity\CouponInterface $promotion_or_coupon
   *   The local discount entity.
   *
   * @return bool
   *   Whether the dates match.
   */
  protected function endDatesMatch(Coupon $recurly_coupon, PromotionInterface|CouponInterface $promotion_or_coupon): bool {
    $end_datetime = $this->getRecurlyEndDateTime($recurly_coupon);
    $recurly_end_timestamp = $end_datetime?->getTimestamp();
    $promo_end_timestamp = $promotion_or_coupon->getEndDate()?->getTimestamp();
    return $recurly_end_timestamp === $promo_end_timestamp;
  }

  /**
   * Syncs remote to local end date if they do not match.
   *
   * @param \Recurly\Resources\Coupon $recurly_coupon
   *   The recurly coupon.
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface|\Drupal\commerce_promotion\Entity\CouponInterface $promotion_or_coupon
   *   The local discount entity.
   *
   * @return PromotionInterface|CouponInterface
   *   The updated promotion or coupon, still must be saved.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function syncStaleEndDate(Coupon $recurly_coupon, PromotionInterface|CouponInterface $promotion_or_coupon): PromotionInterface|CouponInterface {
    if ($this->endDatesMatch($recurly_coupon, $promotion_or_coupon)) {
      return $promotion_or_coupon;
    }

    $end_datetime = $this->getRecurlyEndDateTime($recurly_coupon);
    $promotion_or_coupon->setEndDate($end_datetime);
    return $promotion_or_coupon;
  }


  /**
   * Get the DrupalDateTime for the coupon end date.
   *
   * @param \Recurly\Resources\Coupon $recurly_coupon
   *   The recurly coupon.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   The end DrupalDateTime, or null.
   */
  protected function getRecurlyEndDateTime(Coupon $recurly_coupon): ?DrupalDateTime {
    $end_date = $recurly_coupon->getRedeemBy() ?: $recurly_coupon->getExpiredAt();
    return $end_date
      ? $this->stringToDateTime($end_date)
      : NULL;
  }

  /**
   * Generate the commerce coupons for a recurly coupon.
   *
   * These coupons are stored on the given promotion, which
   * should have also been created from this coupon.
   *
   * @param \Recurly\Resources\Coupon $recurly_coupon
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function generateCommerceCoupons(Coupon $recurly_coupon, PromotionInterface $promotion) {
    $codes_list_iterator = $this->client->listUniqueCouponCodes($recurly_coupon->getId());
    $coupon_codes = $recurly_coupon->getCouponType() === 'bulk'
      ? collect($this->collectPagerResults($codes_list_iterator))
        ->map(fn(UniqueCouponCode $unique_coupon_code) => [
          'code' => $unique_coupon_code->getCode(),
          'redeemable' => $unique_coupon_code->getState() === 'redeemable',
          'usage_limit' => 1,
          'usage_limit_customer' => NULL,
        ])
        ->toArray()
      : [
        [
          'code' => $recurly_coupon->getCode(),
          'redeemable' => $recurly_coupon->getState() === 'redeemable',
          'usage_limit' => $recurly_coupon->getMaxRedemptions(),
          'usage_limit_customer' => $recurly_coupon->getMaxRedemptionsPerAccount(),
        ],
      ];

    foreach ($coupon_codes as $coupon_metadata) {
      $existing_matching_coupons = $this->couponManager->getQuery()
        ->condition('code', $coupon_metadata['code'])
        ->execute();

      if (!empty($existing_matching_coupons)) {
        /** @var CouponInterface $coupon */
        $coupon = $this->couponManager->load(reset($existing_matching_coupons));
        $coupon->set('status', $coupon_metadata['redeemable']);
        $this->syncStaleEndDate($recurly_coupon, $coupon)
          ->save();
        continue;
      }

      /** @var \Drupal\commerce_promotion\Entity\CouponInterface $drupal_coupon */
      $drupal_coupon = $this->couponManager->create([
        'promotion_id' => $promotion->id(),
        'code' => $coupon_metadata['code'],
        'usage_limit' => $coupon_metadata['usage_limit'],
        'usage_limit_customer' => $coupon_metadata['usage_limit_customer'],
        'status' => $coupon_metadata['redeemable'],
      ]);

      $this->syncDates($recurly_coupon, $drupal_coupon);
      $drupal_coupon->save();
    }
  }

  /**
   * Collect a recurly pager items into an array.
   *
   * \Recurly\Pager implements empty versions of the
   * next() and key() \Iterator methods, meaning that
   * iterator_to_array doesn't work correctly.
   *
   * @param \Recurly\Pager $pager
   *   The pager.
   *
   * @return mixed[]
   *   The pager as an array.
   */
  private function collectPagerResults(Pager $pager) {
    $els = [];
    foreach ($pager as $v) {
      $els[] = $v;
    }
    return $els;
  }

}
