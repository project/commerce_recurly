<?php

namespace Drupal\commerce_recurly;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Helps to determine which events should be dispatched for notifications.
 *
 * @package Drupal\recurly
 */
interface RecurlyWebhookEventDispatcherInterface {

  /**
   * Dispatches the event associated with the given notification.
   *
   * Notification types are all named in format type_notification,
   * e.g. billing_info_updated_notification. The names of consts
   * used for events (should) correspond directly with the type.
   *
   * @param \Recurly_PushNotification $notification
   *   The Recurly push notification for which we are
   *   firing an event.
   * @param string $subdomain
   *   The recurly subdomain associated with the notification.
   *
   * @return ?\Symfony\Contracts\EventDispatcher\Event
   *   The event.
   */
  public function dispatchEvent(\Recurly_PushNotification $notification, string $subdomain): ?Event;

}
