<?php

namespace Drupal\commerce_recurly\Plugin\Commerce\EntityTrait;

use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides an entity trait for Commerce Order Item entities.
 *
 * Product variations that correspond with a plan that has been set up
 * in Recurly must use this trait in order to help ensure that the purhase
 * activates the given Plan Subscription on the customer's account.
 *
 * @see Drupal\commerce_recurly\Plugin\Commerce\PaymentGateway\Recurly
 * @see commerce_recurly_entity_bundle_field_info_alter()
 *
 * @CommerceEntityTrait(
 *  id = "commerce_recurly_plan_code",
 *  label = @Translation("Provides a Recurly Subscription based on its Plan Code."),
 *  entity_types = {"commerce_product_variation"}
 * )
 */
class RecurlyPlanVariation extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    // Builds the field definitions.
    $fields = [];
    $fields['plan_code'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Recurly Plan Code'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);
    $fields['validate_plan_code'] = BundleFieldDefinition::create('boolean')
      ->setLabel(t('Validate Recurly Plan Code'))
      ->setCardinality(1)
      ->setRequired(FALSE)
      ->setDefaultValue(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);
    return $fields;
  }

}

