<?php

namespace Drupal\commerce_recurly\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_recurly\PurchaseHelper;
use Drupal\commerce_recurly\RecurlyClient;
use Drupal\commerce_recurly\Traits\AccountTrait;
use Drupal\commerce_recurly\Traits\ApiCredentialsTrait;
use Drupal\commerce_recurly\Traits\CustomFieldsTrait;
use Drupal\commerce_recurly\Traits\GatewayConfigFormTrait;
use Drupal\commerce_recurly\Utils;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Utility\Token;
use Recurly\Errors\NotFound;
use Recurly\RecurlyError;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a base class for RecurlyJs Offsite Gateways.
 *
 * Note that all functionality shared between the different
 * types of gateways, such config form fields and getters,
 * is defined in Traits and used below.
 *
 * All offsite Gateways leveraging RecurlyJs essentially need to
 * use the exact same logic as one-another once RecurlyJs returns
 * a token to be used for payment processing. In order to provide
 * proper logical encapsulation, though, each implementation
 * (paypal, apple pay, roku, etc) should be given their own gateway.
 *
 */
abstract class RecurlyJsOffsiteBase extends OffsitePaymentGatewayBase implements OffsitePaymentGatewayInterface, RecurlyCommonGatewayInterface {

  use AccountTrait;
  use ApiCredentialsTrait;
  use CustomFieldsTrait;
  use GatewayConfigFormTrait;

  /**
   * Recurly client service.
   *
   * @var \Drupal\commerce_recurly\RecurlyClient
   */
  protected $recurlyClient;

  /**
   * The client instance.
   *
   * @var \Recurly\Client
   */
  protected $recurlyClientInstance;

  /**
   * Whether or not recurly module is enabled.
   *
   * @var bool
   */
  protected $recurlyModuleExists;

  /**
   * Recurly module config settings, if module is enabled.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $recurlyModuleConfig;

  /**
   * Array of Recurly "custom fields" defined in module settings.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $configCustomFields;

  /**
   * Entity type bundle info service.
   *
   * @var Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Logger channel factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerChannelFactory;

  /**
   * Commerce_recurly logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The commerce_recurly utils service.
   *
   * @var \Drupal\commerce_recurly\Utils
   */
  protected $commerceRecurlyUtils;

  /**
   * @var \Drupal\commerce_recurly\PurchaseHelper
   */
  private PurchaseHelper $purchaseHelper;

  /**
   * RecurlyJsOffsiteBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\commerce_recurly\RecurlyClient $recurly_client
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   * @param \Drupal\Core\Utility\Token $token
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_channel_factory
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   * @param \Drupal\commerce_recurly\Utils $commerce_recurly_utils
   */
  public function __construct(
    array                      $configuration,
                               $plugin_id,
                               $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager         $payment_type_manager,
    PaymentMethodTypeManager   $payment_method_type_manager,
    TimeInterface              $time,
    MessengerInterface         $messenger,
    RecurlyClient              $recurly_client,
    ModuleHandler              $module_handler,
    ConfigFactory              $config_factory,
    EntityTypeBundleInfo       $entity_type_bundle_info,
    Token                      $token,
    LoggerChannelFactory       $logger_channel_factory,
    EntityFieldManager         $entity_field_manager,
    Utils                      $commerce_recurly_utils,
    PurchaseHelper             $purchase_helper
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->messenger = $messenger;
    $this->recurlyClient = $recurly_client;
    $this->recurlyModuleExists = $module_handler->moduleExists('recurly');
    if ($this->recurlyModuleExists) {
      $this->recurlyModuleConfig = $config_factory
        ->get('recurly.settings');
    }

    $this->configCustomFields = $config_factory
      ->get('commerce_recurly.settings')
      ->get('custom_fields');

    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->token = $token;
    $this->loggerChannelFactory = $logger_channel_factory;
    $this->logger = $logger_channel_factory->get('commerce_recurly');
    $this->entityFieldManager = $entity_field_manager;
    $this->commerceRecurlyUtils = $commerce_recurly_utils;
    $this->purchaseHelper = $purchase_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('commerce_recurly.recurly_client'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('entity_type.bundle.info'),
      $container->get('token'),
      $container->get('logger.factory'),
      $container->get('entity_field.manager'),
      $container->get('commerce_recurly.utils'),
      $container->get('commerce_recurly.purchase_helper'),
    );
  }

  /**
   * {@inheritDoc}
   *
   * @see \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $this->initClient();

    try {
      // We don't actually leverage stored drupal payment methods
      // for our offsite gateways, so we instead just associate
      // a single pattern with the gateway and use the associated
      // recurly account in the same way that we would use a payment
      // method account.
      $payment_method_account = $this->getAssociatedNonplanAccount($order);
      $payment_form_data = $request->request->get('payment_process');
      $this->recurlyClientInstance->updateBillingInfo(
        $payment_method_account->getId(),
        ["token_id" => $payment_form_data['offsite_payment']['recurly_token_id']]
      );

      $purchase = $this->purchaseHelper->doRecurlyPurchase($order, NULL, $this->configuration, $payment_method_account->getId());

      /**
       * From commerce_paypal ExpressCheckout::onReturn()
       */
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

      $payment = $payment_storage->create([
        'state' => 'authorization',
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order->id(),
        'remote_id' => $purchase['remote_id'],
        'remote_state' => $purchase['remote_state'],
      ]);

      if ($mapped_state = $this->getMappedState($purchase['remote_state'])) {
        $payment->setState($mapped_state);
      }

      $payment->save();
    }
    catch (RecurlyError $e) {
      $error_msg = '<div>Your purchase could not be completed because:';
      $error_msg .= "<ul>";
      foreach ($e->getApiError()->getParams() as $param) {
        $error_msg .= "<li>{$param->message}</li>";
      }
      $error_msg .= "</ul></div>";

      $this->messenger->addError($this->t($error_msg));
      throw new PaymentGatewayException($this->t('Purchase could not be completed. There was an unexpected error at the payment gateway. Please try again later.'));
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Purchase could not be completed: ' . $e->getMessage()));
      throw new PaymentGatewayException($e->getMessage());
    }
  }

  protected abstract function getNonplanAccountCode(OrderInterface $order);

  /**
   * Ensure the "bill to" account exists for the gateway.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order object being charged.
   *
   * @param bool $create_missing_account
   *   (optional) Whether the account should be create if one is
   *   not found. Defaults to FALSE.
   *
   * @return \Recurly\Resources\Account|null
   */
  private function getAssociatedNonplanAccount(OrderInterface $order, bool $create_missing_account = TRUE) {
    $account_code = $this->getNonplanAccountCode($order);
    $account_id = "code-$account_code";

    try {
      $this->initClient();
      return $this->recurlyClientInstance->getAccount($account_id);
    }
    catch (NotFound $e) {
      if (!$create_missing_account) {
        return NULL;
      }

      // Ideally would add first_name / last_name if present
      return $this->recurlyClientInstance->createAccount([
        'code' => $account_code,
        'email' => $order->getEmail(),
        'custom_fields' => $this->generateRecurlyCustomFieldData($order),
      ]);
    }
    catch (\Exception $e) {
      throw new HardDeclineException('There was an issue communicating with the payment processor. You have not been charged. Please try again later.');
    }
  }

  /**
   * Returns a mapping of Recurly Invoice states to payment states.
   *
   * There's not really a 1-to-1 relationship that I can see right now,
   * so we just have some rudimentary handling atm.
   *
   * @see https://developers.recurly.com/api/v2019-10-10/index.html#operation/get_invoice
   *   "open" "pending" "processing" "past_due" "paid" "closed" "failed"
   *   "voided"
   *
   * @param string $state
   *   The Recurly Invoice state.
   *
   * @return string
   *   The commerce payment state that matches the given invoice state
   *   if a match is defined.
   */
  protected function getMappedState($state) {
    $mapping = [
      'voided' => 'authorization_voided',
      'pending' => 'authorization',
      'processing' => 'authorization',
      //      'past_due'
      // 'failed'
      'paid' => 'completed',
      'closed' => 'completed',
    ];

    return isset($mapping[$state]) ? $mapping[$state] : '';
  }


}
