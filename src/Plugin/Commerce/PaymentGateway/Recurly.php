<?php

namespace Drupal\commerce_recurly\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_recurly\PurchaseHelper;
use Drupal\commerce_recurly\RecurlyClient;
use Drupal\commerce_recurly\Traits\AccountTrait;
use Drupal\commerce_recurly\Traits\ApiCredentialsTrait;
use Drupal\commerce_recurly\Traits\CustomFieldsTrait;
use Drupal\commerce_recurly\Traits\GatewayConfigFormTrait;
use Drupal\commerce_recurly\Utils;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Utility\Token;
use Exception;
use Recurly\Errors\NotFound;
use Recurly\RecurlyError;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Recurly payment gateway.
 *
 * Note that all functionality shared between the different
 * types of gateways, such config form fields and getters,
 * is defined in Traits and used below.
 *
 * @CommercePaymentGateway(
 *   id = "recurly",
 *   label = "Recurly",
 *   display_label = "Recurly",
 *   forms = {
 *     "add-payment-method" =
 *   "Drupal\commerce_recurly\PluginForm\Recurly\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 *   js_library = "commerce_recurly/checkout",
 * )
 */
class Recurly extends OnsitePaymentGatewayBase implements OnsitePaymentGatewayInterface, SupportsAuthorizationsInterface, RecurlyCommonGatewayInterface {

  use AccountTrait;
  use ApiCredentialsTrait;
  use CustomFieldsTrait;
  use GatewayConfigFormTrait;

  /**
   * Recurly client service
   *
   * @var \Drupal\commerce_recurly\RecurlyClient
   */
  protected $recurlyClient;

  /**
   * @var \Recurly\Client
   */
  protected $recurlyClientInstance;

  /**
   * Whether or not https://www.drupal.org/project/recurly
   * exists on the site and is enabled.
   *
   * @var bool
   */
  protected $recurlyModuleExists;

  /**
   * Recurly module config settings, if module is enabled.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $recurlyModuleConfig;

  /**
   * Array of Recurly "custom fields" defined in module settings.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $configCustomFields;

  /**
   * @var EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The token service
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Logger channel factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerChannelFactory;

  /**
   * Commerce_recurly logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The commerce_recurly utils service.
   *
   * @var \Drupal\commerce_recurly\Utils
   */
  protected $commerceRecurlyUtils;

  /**
   * @var \Drupal\commerce_recurly\PurchaseHelper
   */
  private PurchaseHelper $purchaseHelper;

  /**
   * Recurly constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\commerce_recurly\RecurlyClient $recurly_client
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   * @param \Drupal\Core\Utility\Token $token
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_channel_factory
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   * @param \Drupal\commerce_recurly\Utils $commerce_recurly_utils
   */
  public function __construct(
    array                      $configuration,
                               $plugin_id,
                               $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager         $payment_type_manager,
    PaymentMethodTypeManager   $payment_method_type_manager,
    TimeInterface              $time,
    MessengerInterface         $messenger,
    RecurlyClient              $recurly_client,
    ModuleHandler              $module_handler,
    ConfigFactory              $config_factory,
    EntityTypeBundleInfo       $entity_type_bundle_info,
    Token                      $token,
    LoggerChannelFactory       $logger_channel_factory,
    EntityFieldManager         $entity_field_manager,
    Utils                      $commerce_recurly_utils,
    PurchaseHelper             $purchase_helper
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->messenger = $messenger;
    $this->recurlyClient = $recurly_client;
    $this->recurlyModuleExists = $module_handler->moduleExists('recurly');
    if ($this->recurlyModuleExists) {
      $this->recurlyModuleConfig = $config_factory
        ->get('recurly.settings');
    }

    $this->configCustomFields = $config_factory
      ->get('commerce_recurly.settings')
      ->get('custom_fields');

    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->token = $token;
    $this->loggerChannelFactory = $logger_channel_factory;
    $this->logger = $logger_channel_factory->get('commerce_recurly');
    $this->entityFieldManager = $entity_field_manager;
    $this->commerceRecurlyUtils = $commerce_recurly_utils;
    $this->purchaseHelper = $purchase_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('commerce_recurly.recurly_client'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('entity_type.bundle.info'),
      $container->get('token'),
      $container->get('logger.factory'),
      $container->get('entity_field.manager'),
      $container->get('commerce_recurly.utils'),
      $container->get('commerce_recurly.purchase_helper'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\commerce_stripe\ErrorHelper
   *   A cleaner way to convert Recurly errors into Commerce errors.
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    if (!$capture) {
      throw new InvalidRequestException($this->t('The selected Recurly payment gateway does not currently support Authorization-only transactions.'));
    }

    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    assert($payment_method instanceof PaymentMethodInterface);
    $this->assertPaymentMethod($payment_method);
    $order = $payment->getOrder();
    assert($order instanceof OrderInterface);

    $initial_bill_to_account_id = $payment_method->getRemoteId();
    if (!$initial_bill_to_account_id) {
      $last_four = $payment_method->get('card_number')->value;
      throw new HardDeclineException(sprintf('The card %s did not have a valid remote billing target.', $last_four));
    }

    try {
      $purchase_data = $this->purchaseHelper->doRecurlyPurchase($order, $payment_method, $this->configuration);
    }
    catch (RecurlyError $e) {
      $error_msg = '<div>Your purchase could not be completed because:';
      $error_msg .= "<ul>";
      foreach ($e->getApiError()->getParams() as $param) {
        $error_msg .= "<li>{$param->message}</li>";
      }
      $error_msg .= "</ul></div>";

      $this->messenger->addError($this->t($error_msg));
      throw new PaymentGatewayException($this->t('Purchase could not be completed. There was an unexpected error at the payment gateway. Please try again later.'));
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Purchase could not be completed: ' . $e->getMessage()));
      throw new PaymentGatewayException($e->getMessage());
    }

    // If the order was $0 then there will not be an associated
    // transaction. This happens when the Drupal order has a
    // total of $0.
    if (empty($purchase_data['remote_id'])) {
      $payment->delete();
      return;
    }

    $next_state = $capture ? 'completed' : 'authorization';
    $payment->setState($next_state);
    $payment->setRemoteId($purchase_data['remote_id']);
    $payment->save();

  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {

    $this->assertPaymentState($payment, ['authorization']);
    throw new PaymentGatewayException($this->t('The Recurly payment gateway does not currently support Authorization transactions'));

    //    $payment->setState('completed');
    //    $payment->setAmount($amount);
    //    $payment->save();
  }

  /**
   * {@inheritdoc}
   *
   * @todo: Need to actually add handling for this. Right now does nothing.
   */
  public function voidPayment(PaymentInterface $payment) {
    throw new Exception($this->t('Voiding Recurly payments is not currently supported.'));
    //    $this->assertPaymentState($payment, ['authorization']);
    //    // Void Stripe payment - release uncaptured payment.
    //    try {
    //      $remote_id = $payment->getRemoteId();
    //      $charge = \Stripe\Charge::retrieve($remote_id);
    //      $intent_id = $charge->payment_intent;
    //
    //      if (!empty($intent_id)) {
    //        $intent = \Stripe\PaymentIntent::retrieve($intent_id);
    //        $statuses_to_void = [
    //          'requires_payment_method',
    //          'requires_capture',
    //          'requires_confirmation',
    //          'requires_action',
    //        ];
    //        if (!in_array($intent->status, $statuses_to_void)) {
    //          throw new PaymentGatewayException('The PaymentIntent cannot be voided.');
    //        }
    //        $intent->cancel();
    //      }
    //      else {
    //        $data = [
    //          'charge' => $remote_id,
    //        ];
    //        // Voiding an authorized payment is done by creating a refund.
    //        $release_refund = \Stripe\Refund::create($data);
    //        ErrorHelper::handleErrors($release_refund);
    //      }
    //    } catch (\Stripe\Exception\ApiErrorException $e) {
    //      ErrorHelper::handleException($e);
    //    }
    //
    //    $payment->setState('authorization_voided');
    //    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      //          'stripe_payment_method_id',
      'field_recurly_token',
      'field_payment_method_owner_uid',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new InvalidRequestException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    $remote_payment_method = $this->doCreatePaymentMethod($payment_method, $payment_details);

    // May need extra mapping for card types if errors crop up
    $payment_method->card_type = $this->mapCreditCardType($remote_payment_method['card_type']);
    $payment_method->card_number = $remote_payment_method['last4'];
    $payment_method->card_exp_month = $remote_payment_method['exp_month'];
    $payment_method->card_exp_year = $remote_payment_method['exp_year'];
    $expires = CreditCard::calculateExpirationTimestamp($remote_payment_method['exp_month'], $remote_payment_method['exp_year']);
    // The ID of the billing info object itself is not actionable.
    // Since one payment method is attached to any given user,
    // the Account ID (not account code) itself is used as the remote ID.
    $payment_method->setRemoteId($remote_payment_method['account_id']);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * Creates the payment method on the gateway.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param array $payment_details
   *   The gateway-specific payment details.
   *
   * @return array
   *   The payment method information returned by the gateway. Notable keys:
   *   - token: The remote ID.
   *   Credit card specific keys:
   *   - card_type: The card type.
   *   - last4: The last 4 digits of the credit card number.
   *   - expiration_month: The expiration month.
   *   - expiration_year: The expiration year.
   */
  protected function doCreatePaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    // Right now we are assuming that the method is being added
    // from a checkout flow that will be able to provide an Order ID.
    // Additional accommodations must be made to handle creation of
    // payment methods in arbitrary contexts.
    $order = $this->entityTypeManager
      ->getStorage('commerce_order')
      ->load($payment_details['field_order_id']);

    // Always use nonplan accounts for representing payment methods
    $account_code_pattern = $this->getAccountCodePattern('nonplan');

    // Determine the de-tokenized account ID
    $account_code = $this->token
      ->replace($account_code_pattern, ['commerce_order' => $order]);

    $billing_profile = $payment_method->getBillingProfile();
    if ($billing_profile) {
      $billing_address = $billing_profile->get('address')->first()->toArray();
      $first_name = $billing_address['given_name'];
      $last_name = $billing_address['family_name'];
    }
    elseif (isset($payment_details["recurly_name"]["first_name"], $payment_details["recurly_name"]["last_name"])) {
      $first_name = $payment_details["recurly_name"]["first_name"];
      $last_name = $payment_details["recurly_name"]["last_name"];
    }
    else {
      throw new HardDeclineException('Could not determine the name associated with your payment method. Please try again or contact support.');
    }

    $this->initClient();

    $delta_arr = $this->commerceRecurlyUtils
      ->determineNonplanDelta($account_code);
    $account_code = $this->constructNonplanAccountCode($account_code, $delta_arr['delta']);

    try {
      if ($delta_arr['account_action'] === 'create') {
        $account_data = [
          'code' => $account_code,
          'email' => $payment_method
            ->getOwner()
            ->getEmail(),
          'first_name' => $first_name,
          'last_name' => $last_name,
        ];

        $custom_fields = $this->generateRecurlyCustomFieldData($order);
        if (!empty($custom_fields)) {
          $account_data['custom_fields'] = $custom_fields;
        }

        $this->recurlyClientInstance->createAccount($account_data);
      }

      // Attach the billing info to the account
      $this->recurlyClientInstance->updateBillingInfo(
        "code-{$account_code}",
        ["token_id" => $payment_details['field_recurly_token']]
      );

      // Cannot use the result of the above as it doesn't contain
      // the required info until after it's attached to the account.
      $recurly_billing_info = $this->recurlyClientInstance->getBillingInfo("code-$account_code");
      $recurly_payment_method = $recurly_billing_info->getPaymentMethod();

      return [
        'account_id' => $recurly_billing_info->getAccountId(),
        'card_type' => $recurly_payment_method->getCardType(),
        'last4' => $recurly_payment_method->getLastFour(),
        'exp_month' => $recurly_payment_method->getExpMonth(),
        'exp_year' => $recurly_payment_method->getExpYear(),
      ];
    }
    catch (RecurlyError $e) {
      $this->logger->error($e->getMessage());
      throw new HardDeclineException('There was an issue adding your payment method. Please try again.');
    }
    catch (Exception $e) {
      $this->logger->error($e->getMessage());
      throw new HardDeclineException('There was an issue adding your payment method. Please try again.');
    }
  }

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $a = NULL;
    // We don't want to delete the customer itself, so we just remove this
    // payment method's billing info from its customer.
    // We could also deactivate the customer but that seems
    // heavy-handed.
    $this->initClient();
    $payment_method_remote_id = $payment_method->getRemoteId();

    try {
      // We only want to remove the billing information from the payment method
      // account. The related Plan account should be set as "bill to parent",
      // but in the event that it had billing info set separately (possibly
      // through the Recurly admin interface or the Recurly module) it should be
      // left intact.
      $this->recurlyClientInstance->getAccount($payment_method_remote_id);
      $this->recurlyClientInstance->removeBillingInfo($payment_method_remote_id);
    }
    catch (NotFound $e) {
      // nothing to do, as this means either the account doesn't
      // exist or it has no billing info. In either case, there's
      // no billing info to delete so we just move on.
    }
    catch (Exception $e) {
      throw $e;
    }

    $payment_method->delete();
  }

  /**
   * Maps the Recurly credit card type to a Commerce credit card type.
   *
   * @param string $card_type
   *   The Stripe credit card type.
   *
   * @return string
   *   The Commerce credit card type.
   */
  protected function mapCreditCardType($card_type) {
    $map = [
      'American Express' => 'amex',
      'Diners Club' => 'dinersclub',
      'Discover' => 'discover',
      'JCB' => 'jcb',
      'MasterCard' => 'mastercard',
      'Visa' => 'visa',
    ];
    if (!isset($map[$card_type])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $card_type));
    }

    return $map[$card_type];
  }

}
