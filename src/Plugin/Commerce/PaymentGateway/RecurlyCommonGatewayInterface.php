<?php

namespace Drupal\commerce_recurly\Plugin\Commerce\PaymentGateway;

/**
 * Provides the interface for the Recurly payment gateway.
 */
interface RecurlyCommonGatewayInterface {

  /**
   * Get the subdomain set on the gateway.
   *
   * @return string|null
   *   The subdomain, or null if not set.
   */
  public function getSubdomain(): ?string;

  /**
   * Get the private key set on the gateway.
   *
   * @return string|null
   *   The private key, or null if not set.
   */
  public function getPrivateKey(): ?string;

  /**
   * Get the public key set on the gateway.
   *
   * @return string|null
   *   The public key, or null if not set.
   */
  public function getPublicKey(): ?string;

  /**
   * Gets the account code pattern for the given variant on the gateway.
   *
   * @param string $pattern_variant
   *   The pattern variant being looked up.
   *
   * @return string|null
   *   The account code pattern.
   */
  public function getAccountCodePattern(string $pattern_variant): ?string;

  /**
   * Returns an array of all account code patterns on the gateway.
   *
   * @return array|null
   *   The gateway account code patterns.
   */
  public function getAccountCodePatterns(): ?array;

}
