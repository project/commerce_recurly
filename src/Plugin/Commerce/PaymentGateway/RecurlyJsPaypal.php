<?php


namespace Drupal\commerce_recurly\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides a RecurlyJs Paypal payment gateway
 *
 * @CommercePaymentGateway(
 *   id = "recurly_js_paypal",
 *   label = @Translation("Recurly JS - Paypal"),
 *   display_label = @Translation("PayPal"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_recurly\PluginForm\RecurlyJsPaymentForm"
 *   },
 *   js_library = "commerce_recurly/checkout"
 * )
 */
class RecurlyJsPaypal extends RecurlyJsOffsiteBase {

  protected function getNonplanAccountCode(OrderInterface $order): string {
    // Always use nonplan accounts for representing payment methods
    $account_code_pattern = $this->getAccountCodePattern('nonplan');
    $account_code_raw = $this->token->replace($account_code_pattern, ['commerce_order' => $order]);
    return "$account_code_raw--paypal";
  }

}
