<?php

namespace Drupal\commerce_recurly\Plugin\Validation\Constraint;

use Recurly\RecurlyError;
use Drupal\commerce_recurly\RecurlyClient;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the RecurlyPlanCodeExists constraint.
 */
class RecurlyPlanCodeExistsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  protected $recurlyClient;

  protected $entityTypeManager;

  protected $commercePaymentGatewayManager;

  public function __construct(RecurlyClient $recurly_client, EntityTypeManagerInterface $entity_type_manager) {
    $this->recurlyClient = $recurly_client;
    $this->entityTypeManager = $entity_type_manager;

    $this->commercePaymentGatewayManager = $this->entityTypeManager
      ->getStorage('commerce_payment_gateway');
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_recurly.recurly_client'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if (!$this->shouldValidate($items)) {
      return TRUE;
    }

    if (!$gateways = $this->validateActiveGatewaysPresent($constraint)) {
      return FALSE;
    };

    return $this->validatePlanCode($items, $constraint, $gateways);
  }

  /**
   * Determines whether or not we should run validation.
   *
   * @param \Drupal\Core\Field\FieldItemList $items
   *   The items being validated.
   *
   * @return boolean
   *   Whether or not to run validation.
   */
  private function shouldValidate($items) {
    if (!$entity = $items->getEntity()) {
      return FALSE;
    }

    if (!$entity->hasField('validate_plan_code') ||
      $entity->get('validate_plan_code')->isEmpty()) {
      return FALSE;
    }

    if (!$value = $entity->get('validate_plan_code')->getValue()) {
      return FALSE;
    };

    if ($value[0]['value'] === 0) {
      return FALSE;
    };

    return TRUE;
  }

  /**
   * Gets active Recurly JS payment gateways.
   *
   * @return array
   */
  private function getValidRecurlyGateways() {
    $recurly_js_gateways = [];

    $payment_gateways = $this->commercePaymentGatewayManager
      ->loadMultiple();

    foreach ($payment_gateways as $gateway) {
      if (strpos($gateway->getPluginId(), 'recurly') === FALSE) {
        continue;
      }

      // Skip it if it's not active
      if (!$gateway->status()) {
        continue;
      }

      // Make sure config has a secret key
      // Attempt to init api
      $recurly_js_gateways[] = $gateway;

    }

    return $recurly_js_gateways;
  }

  /**
   * Checks that active gateways exists against which to validate the plan code.
   *
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   The constraint object.
   *
   * @return array|bool
   *   The active gateways if they exist.
   */
  private function validateActiveGatewaysPresent(Constraint $constraint) {
    $gateways = $this->getValidRecurlyGateways();

    if (empty($gateways)) {
      $this->context->addViolation($constraint->noActiveGateways);
      return FALSE;
    }

    return $gateways;
  }

  /**
   * Checks that they plan code exists via active payment gateways.
   *
   * @param \Drupal\Core\Field\FieldItemList $items
   *   The field(s) being validated.
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   The constraint object.
   * @param \Drupal\commerce_payment\Entity\PaymentGateway[] $gateways
   *   An array of the active Recurly JS payement gateways.
   *
   * @return bool
   *   Result of the validation.
   */
  private function validatePlanCode($items, Constraint $constraint, $gateways) {
    foreach ($items as $item) {
      $plan_code = $item->value;

      foreach ($gateways as $gateway) {
        $conf = $gateway->getPluginConfiguration();
        $private_key = $conf['private_key'];
        try {
          $client = $this->recurlyClient->init($private_key);
          if ($plan = $client->getPlan("code-$plan_code")) {
            $plan_code_found = TRUE;
          };
        }
        catch (RecurlyError $e) {
          // No need to react to this. The presence of any RecurlyError
          // just means that we can't assert that the plan code exists.
          continue;
        }
      }

      if (!isset($plan_code_found)) {
        $this->context->addViolation($constraint->planCodeNotFound, ['%plan_code' => $plan_code]);

        return FALSE;
      }
    }

    return TRUE;
  }

}
