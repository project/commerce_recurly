<?php

namespace Drupal\commerce_recurly\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "RecurlyPlanCodeExists",
 *   label = @Translation("Recurly Plan Code Exists", context = "Validation"),
 *   type = "string"
 * )
 */
class RecurlyPlanCodeExists extends Constraint {

  public $noActiveGateways = "You do not have any active Recurly payment gateways configured, so the provided plan code cannot be validated.";

  public $planCodeNotFound = "No plan codes matching <i>%plan_code</i> found in any accounts associated with your active Recurly JS Payment Gateways.";

  // The message that will be shown if the value is not an integer.
  public $notInteger = '%plan_code is not an integer';

  // The message that will be shown if the value is not unique.
  public $notUnique = '%value is not unique';

}

