<?php

namespace Drupal\commerce_recurly;

/**
 * Utils class interface.
 */
interface UtilsInterface {

  /**
   * Determines the delta to use for new nonplan payment method accounts.
   *
   * @param string $account_code
   *   The account code being checked.
   *
   * @return array
   *   The delta and type of account.
   *
   * @throws \Exception
   */
  public function determineNonplanDelta(string $account_code): array;

  /**
   * Gets an array of product variations treated as plans.
   *
   * @return array
   *   The plan product variations.
   *
   * @see \Drupal\commerce_recurly\Plugin\Commerce\EntityTrait\RecurlyPlanVariation
   */
  public function getPlanProductVariations(): array;

}
