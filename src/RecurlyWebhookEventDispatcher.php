<?php

namespace Drupal\commerce_recurly;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\recurly\Event\notifications\account\RecurlyBillingInfoUpdatedEvent;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Helps to determine which events should be dispatched for notifications.
 *
 * @package Drupal\recurly
 */
class RecurlyWebhookEventDispatcher implements RecurlyWebhookEventDispatcherInterface {

  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * RecurlyWebhookEventDispatcher constructor.
   *
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(ContainerAwareEventDispatcher $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritDoc}
   */
  public function dispatchEvent(\Recurly_PushNotification $notification, ?string $subdomain = NULL): ?Event {
    $event_name_const_lower = str_replace('_notification', '', $notification->type);

    // Since the instantiated event class is dynamic, it needs
    // to be called as a variable. This requires the full namespace.
    try {
      $class_name = "Drupal\\commerce_recurly\\Event\\notifications\\" . $this->getClassNamespaceSuffix($event_name_const_lower);
    }
    catch (\Exception $e) {
      // Don't want to throw the error because it breaks the general
      // hook_recurly_process_push_notification invocation.
      \Drupal::logger('commerce_recurly')->error($e->getMessage());
      return NULL;
    }
    $event_class_instance = new $class_name($notification, $subdomain);

    $event_const_name = strtoupper($event_name_const_lower);
    $event_name = constant("Drupal\commerce_recurly\Event\RecurlyWebhookEvents::$event_const_name");

    \Drupal::logger('commerce_recurly')
      ->debug("Dispatching event '$event_name' with class name $class_name.");

    return $this->eventDispatcher->dispatch($event_class_instance, $event_name);
  }

  /**
   * Get the namespace suffix of the event class associated with the webhook notification.
   *
   * @param $event_name_const_lower
   *   The webhook notification name/type without the "_notification" suffix.
   *
   * @return string
   *   The event classname, including namespace.
   *
   * @throws \Exception
   */
  private function getClassNamespaceSuffix($event_name_const_lower) {
    $class_spaces = str_replace('_', ' ', $event_name_const_lower);
    $class_spaces_upper = ucwords($class_spaces);
    $class_upper = str_replace(' ', '', $class_spaces_upper);
    $class_name = "Recurly{$class_upper}Event";

    $namespaces = [
      'account' => [
        'RecurlyBillingInfoUpdatedEvent',
        'RecurlyBillingInfoUpdateFailedEvent',
        'RecurlyCanceledAccountEvent',
        'RecurlyDeletedShippingAddressEvent',
        'RecurlyNewAccountEvent',
        'RecurlyNewShippingAddressEvent',
        'RecurlyUpdatedAccountEvent',
        'RecurlyUpdatedShippingAddressEvent',
      ],
      'payment' => [
        'RecurlySuccessfulPaymentEvent',
      ],
      'subscription' => [
        'RecurlyCanceledSubscriptionEvent',
        'RecurlyExpiredSubscriptionEvent',
        'RecurlyNewSubscriptionEvent',
        'RecurlyPausedSubscriptionRenewalEvent',
        'RecurlyReactivatedAccountEvent',
        'RecurlyRenewedSubscriptionEvent',
        'RecurlyScheduledSubscriptionPauseEvent',
        'RecurlySubscriptionPauseCanceledEvent',
        'RecurlySubscriptionPauseCancelledEvent',
        'RecurlySubscriptionPausedEvent',
        'RecurlySubscriptionPauseModifiedEvent',
        'RecurlySubscriptionResumedEvent',
        'RecurlyUpdatedSubscriptionEvent',
      ],
    ];

    $namespace = '';
    foreach ($namespaces as $k => $namespace_set) {
      if ($namespace) {
        continue;
      }

      if (in_array($class_name, $namespace_set)) {
        $namespace = $k;
      }
    }

    if (!$namespace) {
      throw new \Exception("No namespace found for $class_name. No event will be dispatched.");
    }

    return "$namespace\\$class_name";
  }

}
