<?php

namespace Drupal\commerce_recurly\EventSubscriber;

use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\Component\Datetime\Time;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class WebhookSubscriber
 *
 * @package Drupal\commerce_recurly\EventSubscriber
 */
class OrderSubscriber implements EventSubscriberInterface {

  /**
   * The EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current time UNIX timestamp.
   *
   * @var int
   */
  protected $time;

  /**
   * OrderSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Database\Connection $database
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, Time $time) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->time = $time->getCurrentTime();
  }

  /**
   * Get subscribed events.
   *
   * @inheritdoc
   */
  public static function getSubscribedEvents(): array {
    $events[OrderEvents::ORDER_PAID][] = ['onOrderPaid'];
    return $events;
  }

  /**
   * @param OrderEvent $event
   */
  public function onOrderPaid(OrderEvent $event) {
    /**
     * Need to sync the plan account to recurly module handler.
     * Need to patch recurly module invoices page to also track nonplan account transactions.
     */
    $order = $event->getOrder();

    $uid = $order->getCustomer()->id();
    if (!isset($_SESSION['commerce_recurly'])
      || !isset($_SESSION['commerce_recurly']['account_codes_by_order'])
      || !isset($_SESSION['commerce_recurly']['account_codes_by_order'][$order->id()])) {
      return;
    }

    $account_codes = $_SESSION['commerce_recurly']['account_codes_by_order'][$order->id()];
    foreach ($account_codes as $type => $account_code) {

      $fields = [
        'uid' => $uid,
        'type' => $type,
      ];

      // If it doesn't exist, this will add it.
      // If it does exist, this will overwrite it but the
      // data should really be the same. Saves us doing
      // an extra query to check whether or not the row is
      // already set.
      $this->database
        ->merge('commerce_recurly_accounts')
        ->key('recurly_account_code', $account_code)
        ->fields($fields)
        ->execute();

      if ($type === 'plan') {
        $this->savePlanRecurlyAccount($uid, $account_code);
      }
    }

    unset($_SESSION['commerce_recurly']['account_codes_by_order'][$order->id()]);

  }

  /**
   * Save account while bypassing recurly_account_save.
   *
   * @return \Drupal\Core\Database\StatementInterface|int|null
   * @throws \Exception
   */
  private function savePlanRecurlyAccount($uid, $account_code) {
    $fields = [
      'entity_type' => 'user',
      'entity_id' => $uid,
      'updated' => $this->time,
      'status' => 'active', // Since the plan was just purchased
    ];

    return \Drupal::database()->merge('recurly_account')
      ->key('account_code', $account_code)
      ->fields($fields)
      ->execute();
  }


}
