<?php

namespace Drupal\commerce_recurly\Callback;

use Drupal\Core\Security\TrustedCallbackInterface;

class PreRender implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['addCountryCodeAttributes'];
  }

  /**
   * Element #pre_render callback: adds "data-recurly" to the country_code.
   *
   * This ensures data-recurly is on the hidden or select element for
   * the country code, so that it is properly passed to Stripe.
   *
   * @param array $element
   *   The form element.
   *
   * @return array
   *   The modified form element.
   */
  public static function addCountryCodeAttributes(array $element) {
    $element['country_code']['#attributes']['data-recurly'] = 'country';
    return $element;
  }

}