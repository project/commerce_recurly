<?php

namespace Drupal\commerce_recurly;

use Drupal\Core\Config\ConfigFactoryInterface;
use Recurly\Client;

/**
 * Service implementation of the Recurly Client.
 *
 * @package Drupal\commerce_recurly
 */
class RecurlyClient implements RecurlyClientInterface {

  /**
   * The recurly module config settings.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $recurlyModuleConfig;

  /**
   * RecurlyClient constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->recurlyModuleConfig = $config_factory
      ->get('recurly.settings');
  }

  /**
   * {@inheritDoc}
   */
  public function init(string $private_key): Client {
    return new Client($private_key);
  }

  /**
   * {@inheritDoc}
   */
  public function initDefault(): Client {
    $private_key = $this->recurlyModuleConfig->get('recurly_private_api_key');
    return $this->init($private_key);
  }

}
