<?php

namespace Drupal\commerce_recurly;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_promotion\Entity\Coupon;
use Drupal\commerce_recurly\Traits\AccountTrait;
use Drupal\commerce_recurly\Traits\CustomFieldsTrait;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Recurly\Errors\NotFound;
use Recurly\Resources\Account;

/**
 * Provides utilities to/for commerce_recurly.
 */
class PurchaseHelper implements PurchaseHelperInterface {

  use StringTranslationTrait;
  use AccountTrait;
  use CustomFieldsTrait;

  /**
   * The recurly Client.
   *
   * @var \Recurly\Client
   */
  protected $recurlyClient;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private EntityTypeManager $entityTypeManager;

  // this is provided by AccountTrait
  private array $configuration;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Utility\Token
   */
  private Token $token;

  /**
   * Utils constructor.
   *
   * @param \Drupal\commerce_recurly\RecurlyClient $recurly_client_service
   *   The recurly client service.
   */
  public function __construct(
    RecurlyClient        $recurly_client_service,
    EntityTypeBundleInfo $entity_type_bundle_info,
    EntityFieldManager   $entity_field_manager,
    MessengerInterface   $messenger,
    EntityTypeManager    $entity_type_manager,
    Token                $token) {
    $this->recurlyClient = $recurly_client_service->initDefault();
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->token = $token;
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface|null $payment_method
   * @param array $configuration
   * @param string|NULL $payment_method_account_id
   *
   * @return array{
   *     purchase: \Recurly\Resources\InvoiceCollection,
   *     invoice: \Recurly\Resources\Invoice,
   *     remote_id: ?string,
   *     remote_state: string,
   *   }
   *   purchase: The recurly purchase.
   *   invoice: The invoice associated with the purchase
   *   remote_id: The ID of the purchase transaction. If the purchase was
   *     for $0, like with a trial, this will be null.
   *   remote_state: The state of the remote invoice.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function doRecurlyPurchase(OrderInterface $order, ?PaymentMethodInterface $payment_method, array $configuration, string $payment_method_account_id = NULL) {
    $this->configuration = $configuration;
    if ($payment_method && $payment_method_account_id) {
      throw new \Exception('Only one of payment_method or payment_method_account_id should be provided.');
    }

    if (!$payment_method && !$payment_method_account_id) {
      throw new \Exception('One of payment_method or payment_method_account_id must be provided.');
    }

    $payment_method_account_id ??= $payment_method->getRemoteId();
    $purchase_data = $this->prepPurchaseData($order);

    if (!$purchase_data['subscriptions']->isEmpty()) {
      $updated_plan_account_id = $this->updateBillToParent($payment_method_account_id, $order, $payment_method);
    }

    // If the $updated_plan_account_id is set, it means that there
    // is a subscription/plan purchase associated with this order.
    // When this is the case, we need to ensure that the plan account
    // is being charged so that the plans/subscriptions will be assigned
    // to the plan account. For purchases that do not include any
    // subscriptions, can charge directly to the payment method acct.
    $charge_account_id = $updated_plan_account_id ?? $payment_method_account_id;
    $this->persistAccountCodesInSession($order, $payment_method_account_id, $updated_plan_account_id ?? '');

    $purchase = $this->recurlyClient->createPurchase([
      "currency" => $purchase_data['currency_code'],
      "account" => [
        "id" => $charge_account_id,
      ],
      'line_items' => $purchase_data['nonplan_items']
        ->map(fn(OrderItemInterface $item) => [
          'currency' => $item->getUnitPrice()->getCurrencyCode(),
          'unit_amount' => $item->getUnitPrice()->getNumber(),
          // recurly seems to want this as a float, not cents like would usually be expcted.
          'quantity' => intval($item->getQuantity()),
          'type' => 'charge',
          'description' => $item->label(),
        ])->values(),
      'subscriptions' => $purchase_data['subscriptions']->values(),
      'coupon_codes' => $purchase_data['recurly_coupon_codes']->values(),
    ]);


    /** @var \Recurly\Resources\Invoice $invoice */
    $invoice = $purchase->getChargeInvoice();
    /** @var \Recurly\Resources\Transaction[] $transactions */
    $transactions = $invoice->getTransactions();

    if ($order->hasField('field_recurly_invoice_id')) {
      $order->set('field_recurly_invoice_id', $invoice->getId());
      $should_save_order = TRUE;
    }

    $subscription_ids = $invoice->getSubscriptionIds();
    if ($subscription_ids
      && $order->hasField('field_recurly_subscription_ids')) {
      $order->set('field_recurly_subscription_ids', $subscription_ids);
      $should_save_order = TRUE;
    }

    if (isset($should_save_order)) {
      $order->save();
    }

    // If the purchase was for $0 then there will be no transactions.
    return [
      'purchase' => $purchase,
      'invoice' => $invoice,
      'remote_id' => !empty($transactions)
        ? $transactions[0]->getId()
        : NULL,
      'remote_state' => $invoice->getState(),
    ];

  }

  /**
   * Preps data to be passed along to the recurly purchase.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   The purchase data.
   */
  private function prepPurchaseData(OrderInterface $order): array {
    $order_items_collected = collect($order->getItems());
    $all_items_same_currency_code = $order_items_collected
        ->map(fn(OrderItemInterface $item) => $item->getUnitPrice()->getCurrencyCode())
        ->unique()
        ->count() === 1;

    if (!$all_items_same_currency_code) {
      throw new InvalidRequestException('Order contains products with multiple different currency codes.');
    }

    $nonplan_items = $order_items_collected->filter(fn(OrderItemInterface $item) => !$this->isPlan($item));
    $plan_items = $order_items_collected->filter(fn(OrderItemInterface $item) => $this->isPlan($item));

    /** @var \Illuminate\Support\Collection $subscriptions */
    $subscriptions = $plan_items->map(function (OrderItemInterface $item) {
      $purchased_variation = $item->getPurchasedEntity();
      if ($purchased_variation->get('plan_code')->isEmpty()) {
        // @todo: What to do if it should have a plan code but doesn't?
        throw new InvalidRequestException($this->t('We\'re sorry, we could not complete the purchase. The product was configured incorrectly. Please try again later.'));
      }

      return [
        'plan_code' => $purchased_variation->get('plan_code')->value,
      ];
    });

    $recurly_coupon_codes = collect($order->get('coupons')->referencedEntities())
      ->map(function (Coupon $commerce_coupon) {
        // If it's a coupon that is synced from recurly then the code
        // should match the recurly coupon code.
        $coupon_code = $commerce_coupon->getCode();

        /** @var \Drupal\commerce_promotion\Entity\Promotion $promotion */
        $promotion = $commerce_coupon->getPromotion();

        // If it's mpt a syncd recurly promo. This is cludgey but there's
        // not a good way to field promotions at time of writing.
        if (!str_starts_with($promotion->getName(), "(Recurly)")) {
          // @todo: Handling for native commerce coupons as line items.
          // We may need to run these calculations manually unless commerce
          // has a native way to deal with this.
          throw new \Exception('There was an error processing your coupon. Please contact customer support.');
        }

        return $coupon_code;
      });

    return [
      'currency_code' => $order_items_collected->first()->getUnitPrice()->getCurrencyCode(),
      'nonplan_items' => $nonplan_items,
      'subscriptions' => $subscriptions,
      'recurly_coupon_codes' => $recurly_coupon_codes,
    ];
  }

  /**
   * Checks whether an order item corresponds with a recurly plan.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $item
   *   The order item.
   *
   * @return bool
   *   Whether an order item corresponds with a recurly plan.
   */
  private function isPlan(OrderItemInterface $item): bool {
    return $item->getPurchasedEntity() && $item->getPurchasedEntity()->hasField('plan_code');
  }

  /**
   * Adds account codes to the $_SESSION.
   *
   * This is to allow them to be leveraged in event subscribers.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param string $nonplan_account_id
   *   The nonplan account id.
   * @param string $plan_account_id
   *   The plan account id.
   *
   * @return void
   * @todo Leverage the cache for this instead.
   *
   */
  private function persistAccountCodesInSession(OrderInterface $order, string $nonplan_account_id, string $plan_account_id): void {
    $order_account_ids = [
      'nonplan' => $nonplan_account_id,
      'plan' => $plan_account_id,
    ];

    // Convert the IDs to codes for storage after order is paid.
    $order_account_codes = [];
    foreach ($order_account_ids as $k => $id) {
      $order_account_codes[$k] = !$id ? '' : $this->recurlyClient->getAccount($id)
        ->getCode();
    }
    unset($_SESSION['commerce_recurly']);
    $_SESSION['commerce_recurly']['account_codes_by_order'][$order->id()] = $order_account_codes;
  }

  /**
   * Updates the "bill to" parent for the associated plan account.
   *
   * @param string $payment_method_account_id
   *   The recurly account ID associated with the payment method.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface|null $payment_method
   *   The payment method, if one exists. Offsite gateways might not
   *   provide a payment method, instead just providing the recurly
   *   account code for an account that acts as a payment method acct.
   *
   * @return string
   *   The plan account ID.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @see billPlanAccountToPaymentMethodAccount
   *   The description outlines why this is important.
   *
   */
  private function updateBillToParent(string $payment_method_account_id, OrderInterface $order, ?PaymentMethodInterface $payment_method): string {
    $plan_account_id = 'code-' . $this->getAccountCodeByOrder('plan', $order);
    $updated_plan_account = $this->billPlanAccountToPaymentMethodAccount($plan_account_id, $payment_method_account_id, $order);
    $updated_plan_account_id = $updated_plan_account->getId();

    // Since a Recurly account can only ever have a single billing
    // method, we should never have more than one commerce_payment_method
    // that references a single Plan Account. As such, find any
    // payment methods that were previously associated with the plan
    // account and remove the reference. Do this before executing the
    // following step to avoid removing the new reference.
    $this->removeStalePaymentMethodReferences($updated_plan_account_id);

    if ($payment_method) {
      // Amend our payment method so that it is aware that
      // its billing info is also being used by this Plan Account.
      //
      // Make sure that we append it, not set it directly,
      // in case multiple plan accounts leverage the billing info.
      $payment_method
        ->get('recurly_plan_account_id')
        ->appendItem($updated_plan_account_id);
      $payment_method->save();
    }

    return $updated_plan_account_id;
  }

  /**
   * Attaches nonplan billing info to an order's proper plan account.
   *
   * TL;DR: Don't ever charge subscriptions to a nonplan recurly
   * account -- they just act as pointers to credit cards. If we
   * need to charge for a subscription, copy the billing info from
   * a nonplan account onto the appropriate plan account.
   *
   * ----
   *
   * It is assumed that only one Recurly Customer Account will
   * ever exist for a given Plan pattern. While this is not strictly
   * enforced -- in that an admin could change the pattern at any time --
   * we make this assumption in our internal logic.
   *
   * This helps to:
   *   - avoid situations where a single user ends up with the same
   *     subscription on multiple remote accounts, resulting in
   *     duplicate billing.
   *   - maintain consistency in account code naming for interacting
   *     with recurly's webhooks.
   *
   * This is not the case for nonplan codes. Users may be associated with
   * any number of Recurly customer accounts using nonplan patterns.
   * This is handled by appending deltas to the end of the pattern.
   *
   * However, it is important that we do not ever assign subscriptions
   * to nonplan customer accounts. This is to avoid the duplicate plan
   * assignment issue noted above. To this end, we do not want to allow
   * a payment method associated with a nonplan customer account to be
   * used when charging an order containing a Recurly plan order item.
   *
   * @param string $plan_account_id
   *   The plan account whose bill_to parent is being updated. Note that
   *   in the case of a plan purchase, we need to charge the purchase to
   *   this ID (the plan account id). This will ensure that the plan
   *   account gets assigned the subscriptions/plans as intended while
   *   billing to the payment_method parent account.
   * @param string $payment_method_account_id
   *   The ID of the payment method recurly account. The billing info
   *   on this account will be used to pay for purchases by the plan acct.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Recurly\Resources\Account
   *   The plan account.
   */
  private function billPlanAccountToPaymentMethodAccount(string $plan_account_id, string $payment_method_account_id, OrderInterface $order): Account {
    try {
      $bill_to_parent_account = $this->recurlyClient->getAccount($payment_method_account_id);
      $plan_account = $this->ensurePlanAccountExists($plan_account_id, $bill_to_parent_account, $order);

      $account_update = [
        'parent_account_id' => $payment_method_account_id,
        'bill_to' => 'parent',
      ];

      return $this->recurlyClient->updateAccount($plan_account->getId(), $account_update);
    }
    catch (\Throwable $e) {
      throw new HardDeclineException($e->getMessage());
    }
  }

  /**
   * Disassociates the plan account from its payment methods.
   *
   * We want to do this when updating a plan's bill-to parent
   * because that means that the local payment method associated
   * with the previous bill-to-parent should no longer reference
   * the plan as its child.
   *
   * For example, after 2 plan purchases with payment methods
   *
   *   Plan purchase 1:
   *     - Plan account code: subscription--1
   *     - Bill to account code: payment-method--0
   *     - local payment method: [
   *          id: 1,
   *          remote_id: payment-method--0,
   *          recurly_plan_account_id:  subscription--1
   *       ]
   *
   *   Plan purchase 2:
   *     - Plan account code: subscription--1
   *     - Bill to account code: payment-method--1
   *     - local payment method: [
   *          id: 2,
   *          remote_id: payment-method--1,
   *          recurly_plan_account_id: subscription--1
   *       ]
   *
   *   We end up with two local payment methods referencing
   *   subscription--1 as their recurly_plan_account_id. The
   *   purpose of this method is to ensure that only local
   *   payment method 2 has "recurly_plan_account_id: subscription--1"
   *
   * @param string $plan_account_id
   *   The plan account ID.
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function removeStalePaymentMethodReferences(string $plan_account_id): void {
    $stale_references = $this->entityTypeManager
      ->getStorage('commerce_payment_method')
      ->loadByProperties(['recurly_plan_account_id' => $plan_account_id]);

    foreach ($stale_references as $stale_payment_method) {
      // In case the payment method has multiple child
      // recurly_plan_accounts, we need to loop over the
      // values of the field.
      $account_ids = $stale_payment_method
        ->get('recurly_plan_account_id')
        ->getValue();

      $stale_payment_method
        ->set('recurly_plan_account_id', collect($account_ids)
          ->filter(fn(array $val) => $val['value'] !== $plan_account_id)
          ->values()
          ->all())
        ->save();
    }
  }

  /**
   * Ensures that the plan account exists.
   *
   * Since this is called in the context of assigning a bill-to
   * parent, we reliably have the bill-to account already and can
   * populate some default information from there. If the account
   * already exists, we do not need to do that because the info
   * was presumably set when the plan account was previously
   * created or updated.
   *
   * @param string $prefixed_plan_account_code
   *   The account code for the plan account, which should already be
   *   prefixed with 'code-'.
   * @param \Recurly\Resources\Account $bill_to_parent_account
   *   The recurly account that the plan is being billed to.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   *
   * @return \Recurly\Resources\Account
   *   The plan recurly account.
   */
  private function ensurePlanAccountExists(string $prefixed_plan_account_code, Account $bill_to_parent_account, OrderInterface $order): Account {
    // At some point, a bug was introduced that created accounts with
    // 'code-' at the beginning of their account_code, causing lookups
    // to look like $client->getAccount("code-code-foo"). This was due
    // to confusion regarding the naming of the $plan_account_code
    // param, so code-{$account_code} was passed into here, and
    // then the account was created as code-{code-{$account_code}.
    //
    // Unfortunately, account_code values are immutable in Recurly,
    // so we cannot modify those problematic accounts. Instead, we
    // need to look up both potential values, and address naming
    // of accounts moving forward.
    //
    // So, we now need to look for two potential parent accounts.
    //   1. We'll default to the intended pattern, where 'code-' was
    //      included in the provided parameter.
    //   2. If that account does not exist, then secondarily look
    //      for the double-prefixed version.
    $initial_intended_account = $this->nullablegetAccount($prefixed_plan_account_code);
    if($initial_intended_account) {
      return $initial_intended_account;
    }

    $double_prefixed_account = $this->nullablegetAccount("code-$prefixed_plan_account_code");
    if($double_prefixed_account){
      return $double_prefixed_account;
    }

    // This addresses the double-prefixing issue noted above.
    // We need to remove 'code-' from the string so that it
    // isn't included in the stored account_code identifier.
    $unprefixed_account_code = str_replace('code-', '', $prefixed_plan_account_code);
    $account_data = [
      'code' => $unprefixed_account_code,
      'email' => $bill_to_parent_account->getEmail(),
      'first_name' => $bill_to_parent_account->getFirstName(),
      'last_name' => $bill_to_parent_account->getLastName(),
    ];

    $custom_fields = $this->generateRecurlyCustomFieldData($order);
    if (!empty($custom_fields)) {
      $account_data['custom_fields'] = $custom_fields;
    }

    return $this->recurlyClient->createAccount($account_data);
  }

  /**
   * Trys to get the given account, but returns null on error.
   *
   * @param string $account_id
   *  The account ID (or code- prefixed account_code).
   *
   * @return \Recurly\Resources\Account|null
   *   The account, or null.
   */
  private function nullablegetAccount(string $account_id): ?Account {
    try {
      return $this->recurlyClient->getAccount($account_id);
    }
    catch (NotFound $e) {
      return NULL;
    }
  }


}
