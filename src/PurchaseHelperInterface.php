<?php

namespace Drupal\commerce_recurly;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

interface PurchaseHelperInterface {

  public function doRecurlyPurchase(OrderInterface $order, ?PaymentMethodInterface $payment_method, array $configuration, string $payment_method_account_id = NULL);

}
